# Introduction

The Service Manager container can be configured with help of container `Builder` by passing an associative array to the builder constructor. then builder has all materials set and is ready to configure any service manager container.

The following keys are:

-   `implementations`: will define the services implementation. [read more ...](/@poirot-framework/s/poirot/~/drafts/-M9xmOKILlHKsj6fsday/components/ioc#define-implementations)
    
-   `aliases`: set alias names for a service name. [read more ...](/@poirot-framework/s/poirot/~/drafts/-M9xmOKILlHKsj6fsday/components/ioc#alias-names)
    
-   `initializer_aggregate`: give `InitializerAggregate` object to container.
    
-   `attached_initializers`: attach initializers to initializer aggregator of container. [read more ...](/@poirot-framework/s/poirot/~/drafts/-M9xmOKILlHKsj6fsday/components/ioc#initialize-services)
    
-   `services`: register named services to container. [read more ...](/@poirot-framework/s/poirot/~/drafts/-M9xmOKILlHKsj6fsday/components/ioc#set-service)
    
Configuration source can be any iterable by default, and when it's not `array` should passed to `Builder` with `Builder::parse` method.

```php
$builder = new Builder(Builder::parse($myNoneArraySource));
```

The config source can be imported from php file which should return parsable value.

```php
// services.php
return [
    'services' => [
        'config' => [
            'local_time_zone' => 'Europe/Helsinki',
        ],
    ],
];
```
```php
$builder = new Builder(Builder::from(__DIR__ . '/services.php'));
```

# Overview

Here is an example of how you could configure a service manager:

```php
$configuration = [
    'implementations' => [
        'timezone' => \DateTimeZone::class,
        'serverTime' => \DateTimeImmutable::class,
    ],
    'services' => [
        'config' => [
            'local_time_zone' => 'Europe/Helsinki',
        ],
        'timezone' => factory(function(iServicesContainer $container) {
            return new \DateTimeZone($container->get('config')['local_time_zone']);
        }),
        'serverTime' => instance(\DateTimeImmutable::class, [
            'time' => 'now',
            'timezone' => get('timezone'),
        ])->setSharable(false),
    ],
];
```

Configuration can be passed to builder with setter method:

```php
$builder = new Builder();
$builder->setConfigs($configuration);
```

Set Configuration through constructor:

```php
$builder  =  new  Builder($configuration);
```

# `implementations`

An array of map service name to an implementation FQN or an object, or an array of configuration `Params` object.

_Read_ [_Implementations_](/@poirot-framework/s/poirot/~/drafts/-M9xmOKILlHKsj6fsday/components/ioc#define-implementations) _section if need to know more about what implementations are in general._

```php
$conf = [
   'implementations' => [
      'serviceName' => \stdClass::class,
      new Params([
         'service_name' => 'serviceName',
         'implement' => \stdClass::class
      ])
   ],
];
```

# `aliases`

An array of map service name to an alias name, or an array of configuration `Params` object.

_Read_ [_Aliases_](/@poirot-framework/s/poirot/~/drafts/-M9xmOKILlHKsj6fsday/components/ioc#alias-names) _section if need to know more about what aliases are in general._

```php
$conf = [
  'aliases' => [
     'serviceName' => 'aliasName',
     new Params([
        'service_name' => 'serviceName',
        'aliases' => 'aliasName',
     ])
  ],
];
```

# `attached_events`

‌An array of listener object, a resolver instantiator creating a listener or an array of configuration `Params` object. _Priority_ of listener only can be set through configuration `Params`.

_Read_ [_Initializer_](/@poirot-framework/s/poirot/~/drafts/-M9xmOKILlHKsj6fsday/components/ioc#initialize-services) _section if need to know more about what aliases are in general._

```php
$conf = [
   'attached_initializers' => [
       new MyInitializer,
       new Params([
          'initializer' => new MyOtherInitializer,
          'priority' => 10,
       ])
   ],
];
```
Any resolver can be used as a parameter value to lazy load or instantiate the needed object, in the example below the class FQN is given to the instantiator and the object will be created on build time through this resolver.

```php
use Poirot\Std\ArgumentsResolver\InstantiatorResolver;

$conf = [
   'attached_initializers' => [
      new InstantiatorResolver(DummyInitializer::class),
   ]
];
```

# `services`

An array of map service name to a service object.

_Read_ [_Service_](/@poirot-framework/s/poirot/~/drafts/-M9xmOKILlHKsj6fsday/components/ioc#set-service) _section if need to know more about what aliases are in general._

```php
$conf = [
  'services' => [
     'serviceName' => new DummyService,
  ]
];
```
Any resolver can be used as a parameter value to lazy load or instantiate the needed object, in the example below the class FQN is given to the instantiator and the object will be created on build time through this resolver.

```php
use Poirot\Std\ArgumentsResolver\InstantiatorResolver;

$conf = [
  'services' => [
     'serviceName' => new InstantiatorResolver(
        DummyService::class
     )
  ]
];
```
