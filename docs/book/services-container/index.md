# Introduction

The Service Manager Container can stores services or components (classes), these services can be attained from container during script execution in application which could get benefit from the container provided features in many different ways to setup how the requested service should be created on request.

It can be used to managing classes dependencies and performing dependency injection, configuring objects, defining singleton like objects, lazy loading heavy construction objects and more ...

Service is any object extending `iService` interface which can result on creating any result type on request the service from container.

```php
interface iService
{
    /**
     * Create Expected Service
     *
     * @return mixed
     */
    function createService();

    /**
     * The service which get cached and on each request
     * will not be created
     *
     * @return bool
     */
    function isSharable(): bool;

    /**
     * Check whether service can be replaced by another
     * service with same name in container?
     *
     * @return bool
     */
    function isAllowOverride(): bool;
}
```

# Simple Container Service

As an instance here is the simple `Value` service object will get any php value and will return given value upon request of this service from container.

```php
class Value
    implements iService
{
    protected $value;
    /** @var bool */
    protected $allowOverride = true;
    /** @var bool */
    protected $isSharable = true;

    
    /**
     * Constructor
     *
     * @param mixed $value
     * @param bool  $allowOverride
     * @param bool  $isSharable
     */
    function __construct(
        $value, 
        bool $allowOverride = true, 
        bool $isSharable = true
    ) {
        $this->value = $value;
        $this->setAllowOverride($allowOverride);
        $this->setSharable($isSharable);
    }

    /**
     * Create Service
     *
     * @return mixed
     */
    function createService()
    {
        return $this->value;
    }

    /**
     * Set is service allowed to override
     *
     * @param bool $canReplaced
     *
     * @return $this
     */
    function setAllowOverride(bool $canReplaced = true): self
    {
        $this->allowOverride = $canReplaced;
        return $this;
    }

    /**
     * @inheritDoc
     */
    function isAllowOverride(): bool
    {
        return $this->allowOverride;
    }

    /**
     * Set is service sharable?
     *
     * @param bool $sharable
     *
     * @return $this
     */
    function setSharable(bool $sharable = true): self
    {
        $this->isSharable = $sharable;
        return $this;
    }

    /**
     * @inheritDoc
     */
    function isSharable(): bool
    {
        return $this->isSharable;
    }
}
```

The `isAllowOverride` and `isSharable` methods are instruction methods for container.

The created result on **Sharable** service will be cached in container and on next request will not be created, somehow is also very similar to singleton objects.

When a service is not **AllowOverride** it  simply means that after register the service no more service with the same name can be registered to the container.

# Set Service

Each registered service in container should have a name. service can be triggered to create and attain created result by the given name.

> **Note:** service names inside the container are not case sensitive.

Here the simple [`Value`](/@poirot-framework/s/poirot/~/drafts/-M9xmOKILlHKsj6fsday/components/ioc#understanding-container-service) service is used and registered in container.

```php
$serviceManager = new Container;
$serviceManager->set('myService', new Value(function() {
    echo 'This is my registered service.';
}));
```


### Delegating with container on registering service

A service object which is implementing `iFeatureServiceEvent` can delegate with container events system on registering and is able to attach any listener to the `Event` object.

Two specific event will be triggered on set a service into container, before registering service and after that.

```php
interface iBeforeRegistrationListener
    extends iContainerListener
{
    /**
     * Trigger Before Registering Service To Container
     */
    function __invoke(
       string $serviceName, 
       iService $service, 
       ?iService $registeredService, 
       Container $container): void;
}
```

```php
interface iAfterRegistrationListener
    extends iContainerListener
{
    /**
     * Trigger Before Registering Service To Container
     */
    function __invoke(
        string $serviceName,
        iService $service,
        Container $container): void;
}
```

```php
interface iFeatureServiceEvent
    extends iService
    , iEventListenerAggregate
{
   /**
     * Attach Listeners To Container Events
     *
     * @param Events $containerEvents
     */
    function attachToEvents(Events $containerEvents): void;
}
```

# Get Registered Service

The registered service can be attained from the container by the name which is given to the service upon registering. on getting service depends on the `iService::isSharable` result the service will be created for each request when it has `false` value which is so called **fresh** service otherwise the created service instance will be **cached** and the cached instance will be used on request.

By default, a service created is shared. This means that calling the `get()` method twice for a given service will return exactly the same service. Alternately, you can use the `fresh()` method instead of the `get()` method.

Here the `factory` service is used and the resulted service is value returned from calling the given callable on service creation.

```php
$serviceManager = new Container;

$callable = function() { return time(); };
$sharedService = factory($callable)->setSharable();
$freshService  = factory($callable)->setSharable(false);

$serviceManager->set('mySharedService', $sharedService);
$serviceManager->set('myFreshService', $freshService);

print $serviceManager->get('mySharedService');
print $serviceManager->get('myFreshService');
// 1591713019
// 1591713019
sleep(3);
print $serviceManager->get('mySharedService');
print $serviceManager->get('myFreshService');
// 1591713019
// 1591713022
```

# Get Fresh Service

The `fresh()` method works exactly the same as the `get` method, but never caches the service created, nor uses a previously cached instance for the service.

The registered services object has the ability to instruct container to how should behave when getting service instance from the container with `iService::isSharable` value. if in any time the fresh instance of service is required (new instance of service should be created) the `fresh` method from container can be used.

> **Note:** service created with `refresh` method will not get cached at all.

```php
$sharedService = factory(function() {
    static $calls;
    return ++$calls;
})->setSharable();

$serviceManager = new Container;
$serviceManager->set('mySharedService', $sharedService);

// service will be retrieved and get cached
print $serviceManager->get('mySharedService');
// on second request the cached result will be used
print $serviceManager->get('mySharedService');
// create fresh instance by calling given factory method 
print $serviceManager->fresh('mySharedService');

// 1
// 1
// 2
```


# Check Service Existence

The `has` method will check for the existence of a service or its alias name(s) in the container.

**Note:** Container by default will register itself as a service with name equal to FQN of container class and alias of `iServicesContainer::class`.

```php
$serviceManager = new Container;
if ($serviceManager->has(Container::class))
    echo 'Container Registered Service Name: ' . Container::class;

if ($serviceManager->has(iServicesContainer::class))
    echo 'With Container Service Alias: ' . iServicesContainer::class;

// Container Registered Service Name: Poirot\ServiceManager\Container
// With Container Alias: Poirot\ServiceManager\Interfaces\iServicesContainer
```

# Alias Names

The `setAliases` method provides an alternative name for a registered service. An alias can also be mapped to another alias (it will be resolved recursively).

In this example, `stdAliasB` will be resolved to `stdAlias`, then defined aliases `stdAlias` and `std` both will be resolved to `stdClass::class`, and finally will be created through `Value` service.

```php
$serviceManager = new Container;

$serviceManager->set(\stdClass::class, new Value(new \stdClass));
$serviceManager->setAliases(\stdClass::class, 'stdAlias', 'std');
$serviceManager->setAliases('stdAlias', 'stdAliasB');

$serviceManager->get('stdAliasB');
```

# Define Implementations

An _implementation_ can be defined for a service name before the actual service get registered in container. The implementation can be an FQN to an existing interface or an abstract class or a class or object.

> **Note:** Implementation for a service should be defined before registering the
> actual service object.

```php
$serviceManager = new Container;
$serviceManager->setImplementation('config', \Traversable::class);

$serviceManager->set('config', new Value(new ArrayIterator));
$serviceManager->get('config');
```
When implementation for a service already exists can only replaced with the extended interface or object of the current implementation:

```php
$serviceManager = new Container;
$serviceManager->setImplementation('config', \Traversable::class);
$serviceManager->setImplementation('config', \Iterator::class);

$serviceManager->set('config', new Value(new ArrayIterator));
```

Each defined implementation will create an alias for the given service name to implementation FQN. in the example above both `\Iterator::class` and `\Traversable::class` are aliases for `config` service and can be used to attain service from container.

```php
$serviceManager = new Container;
$serviceManager->setImplementation('config', \Traversable::class);
$serviceManager->setImplementation('config', \Iterator::class);

$serviceManager->set('config', new Value(new ArrayIterator));
$serviceManager->get(\Traversable::class);
```

# Initialize services

An initializer is any callable or any class that implements the interface `iInitializer`. Initializers are executed with Container event system for each service the first time they are created, and can be used to configure service objects to inject additional setter dependencies or other type of configurations as the instance is accessible.

Initializers will called both on either Services object and created object resulted from each service both.

> **Note:** It's recommended to use initializers only to setup `iService` and use
> dependency injection on created services due to can rise code
> complexity and untestability.
> 
> In addition having more initializer will slow the code execution. an
> initializer is run for every instance you create through the service
> manager.

```php
interface iInitializerOfServices
{
    /**
     * Initialize Service
     *
     * @param mixed          $instance
     * @param Container $container
     *
     * @return void
     */
    function __invoke($instance, Container $container): void;
}
```

For example, `Container` itself has default initializer which inject container instance to services objects or resulted created service implemented `iServicesAware` interface.

Events of the container is accessible through a method call:

```php
$serviceManager = new Container;
$serviceManager->events()
  ->onInitialize(new InitializerFactory(function($instance, $container) {
    if ($instance instanceof iServicesAware)
        $instance->setServicesContainer($container);
}));
```

# Make External Services

Any service object can be given to container to use benefit of container to make resulted service from given object.

In this example `\DateTimeImmutable` is instantiated using services registered in container to resolve dependencies which here is _timezone_ argument.

```php
$serviceManager = new Container;
$serviceManager->set('timezone', value(new \DateTimeZone('Europe/Helsinki')));

/** @var \DateTimeImmutable $time */
$time = $serviceManager->make(instance(\DateTimeImmutable::class, [
    'time' => 'now',
    'timezone' => get('timezone')
]));

echo $time->format('Y-m-d H:i:s');
```
