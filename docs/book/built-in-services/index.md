# Introduction

The design logic with out-of-the-box services are that the developer itself is aware of how the services are dependent to each other and they meant to be created. so the services and how they defined the creation process are pulling out of container itself. when dependencies are changed or existing service for any reason not a fit for the application you may want to register this service with new service creator implementation. In nutshell it's not container who decide how to resolve service but it defined on service registration time by developer.

# Factory

A factory get any callable that can be used to create resulted service, it will not do any auto resolving of dependencies. Each factory always receive a `iServicesContainer` argument (which is the `Container` requested creating service).

```php
class MyObjectFactory
{
    function __invoke(iServicesContainer $container)
    {
        $dependency = $container->get(Dependency::class);
        return new MyObject($dependency);
    }
}

$container = new Container();
$container->set('MyService', new Factory(new MyObjectFactory));
```
to ease usage of it `factory` as a function can be used:

```php
use function Poirot\ServiceManager\factory;

$container->set('MyService', factory(new MyObjectFactory));
```

# Instance

When it's needed to resolve dependencies from container to class `__construct` to instantiate class or to a callable which is resulted to create a service the `Instance` service can be used.

You may **type-hint** the dependency in the constructor or use **doc-notation** to define dependencies. Dependencies are resolved when for each type-hinted or notation defined argument container has the same defined _implementations_, _service_ or _alias_ with same FQN as name of the type-hinted class or interface.

## **Resolve Type-Hinted service:**

```php
class ServiceWithTypeHintDependency
{
    public $resolvedDependency;

    function __construct(\stdClass $myStdArgument) {
        $this->resolvedDependency = $myStdArgument;
    }
}

$container = new Container;
$container->setImplementation('myStdClass', \stdClass::class);
$container->set('myStdClass', value(new stdClass));
$container->set('myDependentClass', instance(ServiceWithTypeHintDependency::class));

$container->get('myDependentClass');
```

same behaviour when there is an service name or alias for type-hinted FQN:

```php
$container = new Container;
$container->set(\stdClass::class, value(new stdClass));
$container->set('myDependentClass', instance(ServiceWithTypeHintDependency::class));

$container->get('myDependentClass');
```

resolve to a factory method:

```php
class ServiceWithTypeHintDependency
{
    public $resolvedDependency;

    static function create(\stdClass $myStdService) {
        return new self($myStdService);
    }

    function __construct(\stdClass $myStdArgument) {
        // \stdClass as dependency should be resolved to this class
        $this->resolvedDependency = $myStdArgument;
    }
}

$container = new Container;
$container->set('myStdClass', value(new stdClass));
$container->setAliases('myStdClass', \stdClass::class);
$container->set('myDependentClass', instance(
   [ServiceWithTypeHintDependency::class, 'create']
));

$container->get('myDependentClass');
```

## **Resolve Notation service:**

The notation can be defined after `@param` on the method doc-block by `@Service requiredServiceName` notation mark. In the example below class construct argument `$myStdArgument` will be resolved by `myStdImplementation`.

```php
class ServiceWithNotationDependency
{
    public $resolvedDependency;

    /**
     * Constructor
     *
     * @param \stdClass $myStdArgument @Service myStdImplementation
     */
    function __construct(\stdClass $myStdArgument) {
        $this->resolvedDependency = $myStdArgument;
    }
}

$container = new Container;
$container->set('myStdImplementation', value(new stdClass));
$container->set('myDependentClass', instance(ServiceWithNotationDependency::class));

$container->get('myDependentClass');
```

resolve to a factory method:

```php
class ServiceWithNotationDependency
{
    public $resolvedDependency;

    /**
     * Factory
     *
     * @param \stdClass $myStdService @Service myStdImplementation
     * @return ServiceWithNotationDependency
     */
    static function create(\stdClass $myStdService) {
        return new self($myStdService);
    }

    /**
     * Constructor
     *
     * @param \stdClass $myStdArgument
     */
    function __construct(\stdClass $myStdArgument) {
        $this->resolvedDependency = $myStdArgument;
    }
}

$container = new Container;
$container->set('myStdImplementation', value(new stdClass));
$container->set('myDependentClass', instance(
  [ServiceWithNotationDependency::class, 'create']
));

$container->get('myDependentClass');
```

## Resolve dependencies by given options:

When the instance service is using options can given to resolve the dependencies needed to create the service object. the given options is an array map of type-hinted argument or variable name to the value which is being resolved to given instance. the options could be consist of any other `iService` implementation which will be converted to the service result when it's needed.

here for example get service is used to retrieve an existing service from container and used as value of necessary arguments of construct method.

```php
class ServiceWithNotationDependency
{
    public $resolvedDependency;

    function __construct(\stdClass $myStdArgument) {
        $this->resolvedDependency = $myStdArgument;
    }
}

$container = new Container;
$container->set('myStdImplementation', value(new stdClass));
$container->set('myDependentClass',
   instance(ServiceWithNotationDependency::class, [
       \stdClass::class => get('myStdImplementation'),
       // with lower priority argument name also can be used
       // 'myStdArgument' => fresh('myStdImplementation')
   ])
);

$container->get('myDependentClass');
```

# Fresh

The `fresh` service will retrieve fresh version of given service name by creating new instance. This is the equivalent behaviour of `Container::fresh()` method.

The example here won't work to save request time in real case as `requestTime` service will not triggered and not cached till the first `get()` called:

_there is some workaround events which is mitigated from this example to simplify that._

```php
$container = new Container;
$container->set('requestTime', factory(function () {
    return time();
}));
$container->set('timeNow', fresh('requestTime'));

echo $container->get('requestTime');
sleep(3);
echo $container->get('timeNow');
// 1592160137
// 1592160140
```

# Get

The `get` service will retrieve cached version of given service name if it's created before and create service and cached it if it's not. This is the equivalent behaviour of `Container::get()` method.

The example demonstrate how get can be used to define an alias to other service.

```php
$container = new Container;
$container->set(\stdClass::class, value(new \stdClass));
$container->set('myStdClass', get(\stdClass::class));

$container->get('myStdClass');
```

# Value

It's a simple service which return the given value as a resulted service.

Value here is the registered function invokable:

```php
$container = new Container;
$container->set('dateFormatter', value(function($time) {
    return date('Y-m-d H:i:s', $time);
}));

print $container->get('dateFormatter')
   ->__invoke(time()); // 2020-06-14 19:20:06
```

# Custom Service

When you have services which share a common creation pattern and the functionality is not achievable with default defined services or just simply wanted to create your own service there is always an options to create your service creation object by implementing `iService` or extending `aService`.

Also can read: [Simple Container Service](/@poirot-framework/s/poirot/~/drafts/-M9xmOKILlHKsj6fsday/components/ioc#simple-container-service) for more information.
