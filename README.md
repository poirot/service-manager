# Poirot Service Manager

The component are distributed under the "**Poirot/ServiceManager**" namespace and has a [git repository](https://gitlab.com/poirot), and, for the php components, a light **autoloader** and [composer](https://getcomposer.org/) support.

_All Poirot components following the_ [_PSR-4 convention_](https://www.php-fig.org/psr/psr-4/) _for namespace related to directory structure and so, can be loaded using any modern framework autoloader. All packages are well tested and using modern coding standards._


Poirot Service Manager is implementing Dependency Injection and is a Service Locator. This component intended to simplify dependency injection and object construction in your application. [Martin Fowler's article](http://martinfowler.com/articles/injection.html) has well explained why DI container is useful.

The Implementation Tries to reduce the overall complexity of how services can be defined and to get the required dependencies within a component by decoupling them as individual objects.
Pattern which is used in designing Service Manager will increase testability in the code, thus making it less prone to errors.

```php
// services.php

return [
    'implementations' => [
        'timezone' => \DateTimeZone::class,
        'serverTime' => \DateTimeImmutable::class,
    ],
    'services' => [
        'config' => [
            'local_time_zone' => 'Europe/Helsinki',
        ],
        'timezone' => factory(function(iServicesContainer $container) {
            return new \DateTimeZone($container->get('config')['local_time_zone']);
        }),
        'serverTime' => instance(\DateTimeImmutable::class, [
            'time' => 'now',
            'timezone' => get('timezone'),
        ])->setSharable(false),
    ],
];
```

```php
// index.php

$builder = new Builder(Builder::from(__DIR__ . '/services.php'));
$serviceManager = new Container($builder);

$format = 'Y-m-d H:i:s';
print $serviceManager->get('serverTime')->format($format);
sleep(3);
print $serviceManager->get('serverTime')->format($format);

// 2020-06-06 22:54:38
// 2020-06-06 22:54:41
```

# Documentation

Offline documentation is available on [MkDocs](https://www.mkdocs.org/) to see the offline documentation:
- Install MkDocs. read here about [how to install](https://www.mkdocs.org/#installation)
- From package root directory run `mkdocs serve`
- Visit `http://127.0.0.1:8000/` on your browser