<?php
namespace Poirot\ServiceManager\Traits;

trait tNormalizer
{
    /**
     * Normalize Service Name
     *
     * @param string $name
     *
     * @return string
     */
    protected function _normalizeName(string $name): string
    {
        return strtolower($name);
    }
}
