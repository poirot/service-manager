<?php
namespace Poirot\ServiceManager;

use Poirot\ServiceManager\Container\Events;
use Poirot\ServiceManager\Container\Listeners\InitializerFactory;
use Poirot\ServiceManager\Exceptions\AbstractExceptionsFacade;
use Poirot\ServiceManager\Exceptions\AddingServiceError;
use Poirot\ServiceManager\Exceptions\AliasInvalidArgumentError;
use Poirot\ServiceManager\Exceptions\CreateServiceError;
use Poirot\ServiceManager\Exceptions\CyclingDefinedAliasError;
use Poirot\ServiceManager\Exceptions\DefineImplementationError;
use Poirot\ServiceManager\Exceptions\ServiceNotFoundError;
use Poirot\ServiceManager\Interfaces\Exceptions\iErrorDefineImplementation;
use Poirot\ServiceManager\Interfaces\iService;
use Poirot\ServiceManager\Interfaces\iServicesContainer;
use Poirot\ServiceManager\Interfaces\Pacts\iServicesAware;
use Poirot\ServiceManager\Interfaces\Services\iFeatureServiceAggregate;
use Poirot\ServiceManager\Container\Builder;
use Poirot\ServiceManager\Interfaces\Services\iFeatureServiceEvent;
use Poirot\ServiceManager\Services\Value;
use Poirot\ServiceManager\Traits\tNormalizer;


class Container
    implements iServicesContainer
{
    use tNormalizer;

    /** @var iService[] */
    private $registeredServices = [];
    /** @var iFeatureServiceAggregate[] */
    private $registeredAggregateServices = [];
    /** @var array Service Aliases */
    private $aliases = [];
    /** @var array Service Interfaces Contract */
    private $implementations = [];
    /** @var array shared instances */
    private $cachedCreatedServices = [];

    /** @var Events */
    protected $events;


    /**
     * Construct
     *
     * @param Builder|null $containerBuilder
     * @param Events       $events
     */
    final function __construct(?Builder $containerBuilder = null, Events $events = null)
    {
        if ($events) {
            $events->setServicesContainer($this);
            $this->events = $events;
        }

        $this->_init();

        if (null !== $containerBuilder)
            $containerBuilder->build($this);
    }

    function _init()
    {
        $this->setImplementation(iServicesContainer::class, $this);
        $this->set(self::class, new Value($this, false));
    }


    // Nested Objects:

    /**
     * Events Aggregate
     *
     * @return Events
     */
    function events(): Events
    {
        if ($this->events)
            return $this->events;

        $events = new Events;
        $events->onInitialize(new InitializerFactory(function($instance, $container) {
            if ($instance instanceof iServicesAware)
                $instance->setServicesContainer($container);
        }), PHP_INT_MAX);

        $events->setServicesContainer($this);
        return $this->events = $events;
    }


    // Implementation:

    /**
     * Register a service to container
     *
     * @param string   $serviceName
     * @param iService $service Service
     *
     * @return $this
     * @throws AddingServiceError
     */
    function set(string $serviceName, iService $service): self
    {
        if (empty($serviceName) && $serviceName != '0')
            throw AbstractExceptionsFacade::getAddingServiceError(
                [$service],
                AddingServiceError::ErrorCode_ServiceNameCantBeEmpty
            );

        $registeredService = $this->_hasInternalService($serviceName);
        if ($registeredService && !$registeredService->isAllowOverride())
            throw AbstractExceptionsFacade::getAddingServiceError(
                ['service_name' => $serviceName],
                AddingServiceError::ErrorCode_ReplacingServiceNotAllowed
            );


        if ($service instanceof iFeatureServiceEvent)
            $service->attachToEvents($this->events());


        $this->events()
            ->triggerBeforeServiceRegistration($serviceName, $service, $registeredService);

        $this->_registerService($service, $this->_generateServiceIdFromName($serviceName));

        $this->events()
            ->triggerServiceRegistration($serviceName, $service);

        return $this;
    }

    /**
     * Check whether a service with given name or alias exists or not?
     *
     * @param string $serviceName Service name or alias
     *
     * @return bool
     */
    function has(string $serviceName): bool
    {
        $result = null !== $this->_hasInternalService($serviceName);

        if (! $result) {
            // Looking in Aggregate Feature Services
            $serviceName = $this->_getOriginServiceName($serviceName);
            foreach ($this->registeredAggregateServices as $aggregateService) {
                /** @var iFeatureServiceAggregate $aggregateService */
                if ($aggregateService->canCreateService($serviceName))
                    return true;
            }
        }

        return $result;
    }

    /**
     * @inheritDoc
     */
    function get(string $serviceName)
    {
        $serviceId = $this->_generateServiceIdFromName($serviceName);

        ## Service From Cache:
        #
        if (array_key_exists($serviceId, $this->cachedCreatedServices))
            return $this->cachedCreatedServices[$serviceId];

        $serviceToCreate = $this->_retrieveServiceToCreate($serviceName);
        $serviceInstance = $this->_make($serviceToCreate, $serviceName);
        if ($serviceToCreate->isSharable()) {
            $this->cachedCreatedServices[$serviceId] = $serviceInstance;
        }

        return $serviceInstance;
    }

    /**
     * @inheritDoc
     */
    function fresh(string $serviceName)
    {
        $createdService = $this->_make(
            $this->_retrieveServiceToCreate($serviceName),
            $serviceName
        );

        ## check created service to match with defined implementation interface
        #
        if (null !== $definedImplementation = $this->hasImplementation($serviceName))
        {
            if (! $this->_validateIsSubClassOfImplementation($createdService, $definedImplementation))
                throw AbstractExceptionsFacade::getCreateServiceError(
                    [
                        'service_name' => $serviceName,
                        'created_service' => $createdService,
                        'implementation' => $definedImplementation,
                    ],
                    CreateServiceError::ErrorCode_ServiceInvalidImplementation,
                    );
        }

        return $createdService;
    }

    /**
     * @inheritDoc
     */
    function make(iService $service)
    {
        return $this->_make($service, 'Runtime');
    }

    protected function _make(iService $service, string $serviceName)
    {
        try {
            $createdService = $this->_makeFromService($service);
        } catch(\Exception $exception) {
            throw AbstractExceptionsFacade::getCreateServiceError(['service_name' => $serviceName],
                CreateServiceError::ErrorCode_ServiceCreateError,
                $exception
            );
        }

        if ($createdService === null)
            throw AbstractExceptionsFacade::getCreateServiceError(['service_name' => $serviceName],
                CreateServiceError::ErrorCode_ServiceCreatedNothing);

        return $createdService;
    }

    /**
     * Set Service Implementation Interface Contract
     * service can be retrieved by defined implementation FQCN
     *
     * @param string        $serviceName    Service name or alias
     * @param string|object $implementation Interface or an object indicate implementation
     *
     * @return $this
     * @throws iErrorDefineImplementation
     */
    function setImplementation($serviceName, $implementation): self
    {
        if (! is_object($implementation))
            $implementation = (string) $implementation;

        if (is_string($implementation) && !(interface_exists($implementation) || class_exists($implementation)))
            throw AbstractExceptionsFacade::getImplementationDefineError(
                [
                    'service_name' => $serviceName,
                    'implementation' => $implementation,
                ],
                DefineImplementationError::ErrorCode_InvalidImplementationValue
            );

        if ($this->has($serviceName))
            throw AbstractExceptionsFacade::getImplementationDefineError(
                ['service_name' => $serviceName],
                DefineImplementationError::ErrorCode_ServiceAlreadyDefined
            );


        if (is_object($implementation))
            $implementation = get_class($implementation);

        $serviceId = $this->_generateServiceIdFromName($serviceName);
        if (isset($this->implementations[$serviceId])) {
            if (! $this->_validateIsSubClassOfImplementation($implementation, $this->implementations[$serviceId]))
                throw AbstractExceptionsFacade::getImplementationDefineError(
                    [
                        'service_name' => $serviceName,
                        'implementation' => $this->implementations[$serviceId],
                        'new_implementation' => $implementation,
                    ],
                    DefineImplementationError::ErrorCode_DefinedImplementationCantGetChange
                );
        }

        $this->implementations[$serviceId] = $implementation;
        // set Implementation Aliases to Service name
        $this->setAliases($this->_getOriginServiceName($serviceName), $implementation);
        return $this;
    }

    /**
     * Get Implementation Interface of Service Contract
     *
     * @param string $serviceName Service name or alias
     *
     * @return string|null
     */
    function hasImplementation($serviceName): ?string
    {
        $serviceId = $this->_generateServiceIdFromName($serviceName);
        return $this->implementations[$serviceId] ?? null;
    }

    /**
     * Set Alias Name For Registered Service
     *
     * @param string   $serviceName Registered Service/Alias
     * @param string[] $aliases
     *
     * @return $this
     * @throws AliasInvalidArgumentError
     */
    function setAliases(string $serviceName, string ...$aliases): self
    {
        $normalizedServiceName = $this->_normalizeName($serviceName);

        foreach ($aliases as $alias) {
            $normalizedAliasName   = $this->_normalizeName($alias);

            if ($normalizedAliasName === '' || $normalizedServiceName === '')
                throw AbstractExceptionsFacade::getAliasError(
                    ['alias_name' => $aliases, 'service_name' => $serviceName],
                    AliasInvalidArgumentError::ErrorCode_InvalidAliasValue
                );

            if ($normalizedServiceName === $normalizedAliasName)
                throw AbstractExceptionsFacade::getAliasError(
                    ['alias_name' => $aliases, 'service_name' => $serviceName],
                    AliasInvalidArgumentError::ErrorCode_AliasAndServiceNameAreSame
                );

            $this->aliases[$normalizedAliasName] = $normalizedServiceName;
        }

        return $this;
    }

    // ...

    protected function _makeFromService(iService $inService)
    {
        # Initialize Service for dependencies etc.
        # inject dependencies in Service; iServiceAware services that
        # need instance of service itself.
        $this->events()->triggerInitialize($inService);

        # Retrieve Initialized Instance From Service
        $createdService = $inService->createService();
        if (is_object($createdService))
            $this->events()->triggerInitialize($createdService);

        if ($createdService instanceof iService)
            // Services that return service must continue ...
            return $this->_makeFromService($createdService);

        return $createdService;
    }

    protected function _hasInternalService(string $serviceName): ?iService
    {
        return $this->_hasServiceRegisteredById(
            $this->_generateServiceIdFromName($serviceName)
        );
    }

    /**
     * Get Main Root Service Name From Given Alias Or Service Name
     * if service is a root the given name will return as a result.
     *
     * @param string $baseOrAliasName Service name or alias
     *
     * @return string
     */
    protected function _getOriginServiceName(string $baseOrAliasName): string
    {
        return $this->_getBaseServiceNameFromAliases($baseOrAliasName);
    }

    // ...

    private function _registerService(iService $service, $internalServiceId): void
    {
        switch ($service) {
            case $service instanceof iFeatureServiceAggregate:
                $this->registeredAggregateServices[$internalServiceId] = $service;
                break;
            default:
                $this->registeredServices[$internalServiceId] = $service;
        }
    }

    private function _getBaseServiceNameFromAliases(string $baseOrAliasName): string
    {
        $originName = $baseOrAliasName = $this->_normalizeName($baseOrAliasName);

        $visited = [];
        while (isset($this->aliases[$baseOrAliasName])) {
            $baseOrAliasName = $this->aliases[$baseOrAliasName];

            if (isset($visited[$baseOrAliasName])) {
                throw AbstractExceptionsFacade::getAliasError(
                    ['aliases' => $this->aliases],
                    CyclingDefinedAliasError::ErrorCode_CyclingDefinedAliasesFound
                );
            }

            $visited[$baseOrAliasName] = true;
        }

        return $baseOrAliasName;
    }

    private function _hasServiceRegisteredById(string $serviceId): ?iService
    {
        if (isset($this->registeredServices[$serviceId]))
            return $this->registeredServices[$serviceId];

        return null;
    }

    private function _generateServiceIdFromName(string $name): string
    {
        return $this->_getOriginServiceName($name);
    }

    /**
     * Validate if given service has implemented the implementation definition
     *
     * @param object|string $service
     * @param string $definedImplementation
     *
     * @return bool
     */
    private function _validateIsSubClassOfImplementation($service, string $definedImplementation): bool
    {
        $isMatchWithImplementation = false;
        if (interface_exists($definedImplementation))
            $isMatchWithImplementation = in_array($definedImplementation, class_implements($service));
        elseif (class_exists($definedImplementation))
            // check implementation of extended class
            $isMatchWithImplementation = $service instanceof $definedImplementation
                || is_subclass_of($service, $definedImplementation);

        return $isMatchWithImplementation;
    }

    /**
     * @param string $serviceName
     * @return iService
     * @throws ServiceNotFoundError
     */
    private function _retrieveServiceToCreate(string $serviceName): iService
    {
        if (null === $registeredService = $this->_hasInternalService($serviceName)) {
            // Looking in Aggregate Feature Services
            $serviceName = $this->_getOriginServiceName($serviceName);
            /** @var iFeatureServiceAggregate $aggregateService */
            foreach ($this->registeredAggregateServices as $aggregateService)
                if ($aggregateService->canCreateService($serviceName)) {
                    $registeredService = $aggregateService->withName($serviceName);
                    break;
                }

            if (null === $registeredService)
                throw AbstractExceptionsFacade::getServiceNotFoundError(['service_name' => $serviceName]);
        }

        return $registeredService;
    }
}
