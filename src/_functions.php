<?php
namespace Poirot\ServiceManager;

use Poirot\ServiceManager\Services\Factory;
use Poirot\ServiceManager\Services\Fresh;
use Poirot\ServiceManager\Services\Get;
use Poirot\ServiceManager\Services\Instance;
use Poirot\ServiceManager\Services\Value;


if (! function_exists('Poirot\ServiceManager\factory'))
{
    /**
     * Create Factory Service
     *
     * @param callable $serviceFactory Callable in F(FactoryService $service)
     * @param bool     $allowOverride
     * @param bool     $isSharable
     *
     * @return Factory
     */
    function factory(callable $serviceFactory, bool $allowOverride = true, bool $isSharable = true) {
        return new Factory($serviceFactory, $allowOverride, $isSharable);
    }
}

if (! function_exists('Poirot\ServiceManager\fresh'))
{
    /**
     * Create Fresh Service
     *
     * @param string $serviceToRefresh
     * @param bool   $allowOverride
     *
     * @return Fresh
     */
    function fresh(string $serviceToRefresh, bool $allowOverride = true) {
        return new Fresh($serviceToRefresh, $allowOverride);
    }
}

if (! function_exists('Poirot\ServiceManager\get'))
{
    /**
     * Create Get Service
     *
     * @param string $serviceToGet
     * @param bool   $allowOverride
     *
     * @return Get
     */
    function get(string $serviceToGet, bool $allowOverride = true) {
        return new Get($serviceToGet, $allowOverride);
    }
}

if (! function_exists('Poirot\ServiceManager\instance'))
{
    /**
     * Create Instance Service
     *
     * @param mixed      $instantiable   Instantiable value it might be a method, class, or resolvable reflect
     * @param array|null $instanceParams Instance construct options if it's not resolved by Service manager
     * @param bool       $allowOverride
     * @param bool       $isSharable
     *
     * @return Instance
     */
    function instance($instantiable, array $instanceParams = [], bool $allowOverride = true, bool $isSharable = true) {
        return new Instance($instantiable, $instanceParams, $allowOverride, $isSharable);
    }
}

if (! function_exists('Poirot\ServiceManager\value'))
{
    /**
     * Create Instance Service
     *
     * @param mixed $value
     * @param bool  $allowOverride
     * @param bool  $isSharable
     *
     * @return Value
     */
    function value($value, bool $allowOverride = true, bool $isSharable = true) {
        return new Value($value, $allowOverride, $isSharable);
    }
}
