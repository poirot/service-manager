<?php
namespace Poirot\ServiceManager\Services\Instance\ArgumentsResolver;

use Poirot\ServiceManager\Interfaces\iServicesContainer;
use Poirot\Std\ArgumentsResolver\aResolver;
use Poirot\Std\ArgumentsResolver\CallableResolver;
use Poirot\Std\ArgumentsResolver\InstantiatorResolver;
use function Poirot\Std\flatten;


class InstantiableServicesResolver
    extends aServicesResolver
{
    private $instantiable;
    /** @var CallableResolver|InstantiatorResolver */
    private $_resolverWrapper;


    /**
     * Constructor
     *
     * @param callable|string    $instantiable Instantiable value it might be a method or FQCN string
     * @param iServicesContainer $container
     * @param array              $defaultOptions
     */
    function __construct($instantiable, iServicesContainer $container, array $defaultOptions = [])
    {
        if (!is_callable($instantiable) && !class_exists($instantiable))
            throw new \InvalidArgumentException(sprintf(
                'Given $instantiable:(%s) should be a callable or FQCN to a valid autoloadable class.'
                , flatten($instantiable)
            ));

        $this->instantiable = $instantiable;
        parent::__construct($container, $defaultOptions);
    }

    /**
     * @inheritDoc
     */
    function getReflectionMethod()
    {
        return $this->_resolverWrapper()->getReflectionMethod();
    }

    /**
     * @inheritDoc
     */
    function resolve(array $arguments)
    {
        $resolver = $this->_resolverWrapper();
        if ($resolver instanceof CallableResolver)
            return $resolver->resolve($arguments)->__invoke();

        return $resolver->resolve($arguments);
    }

    /**
     * @inheritDoc
     */
    function getDefaultOptions(): array
    {
        return $this->prepareRequiredServicesAsArguments();
    }

    // ..

    /**
     * Resolve Services As Arguments For a Given Function
     *
     * @return array
     */
    protected function prepareRequiredServicesAsArguments(): array
    {
        $reflectionMethod = $this->getReflectionMethod();

        $availableServiceOptions = parent::getDefaultOptions();
        $servicesMapByAnnotation = $this->_makeServiceToArgumentsMapByAnnotation($reflectionMethod);
        foreach ($reflectionMethod->getParameters() as $reflectionParameter)
        {
            // look for argument as a service in current ioc domain(iCServiceAware injected)
            $parameterName = $reflectionParameter->getName();
            if (isset($servicesMapByAnnotation[$parameterName]))
            {
                $serviceName = $servicesMapByAnnotation[$parameterName]['service_name'];
                if (isset($availableServiceOptions[$serviceName]))
                    // Service name is given as options, no need to fetch it.
                    continue;

                if ($this->servicesContainer()->has($serviceName))
                    $availableServiceOptions[$parameterName] = $this->servicesContainer()->get($serviceName);
            }
            elseif ($reflectionParameter->getType())
            {
                // Try to resolve service by it's implementation definition
                // \Full\Qualified\ClassName::class
                $serviceImplementation = $reflectionParameter->getType()->getName();
                if (! isset($availableServiceOptions[$serviceImplementation])
                    && $this->servicesContainer()->has($serviceImplementation)
                ) $availableServiceOptions[$parameterName] = $this->servicesContainer()->get($serviceImplementation);
            }
        }

        return $availableServiceOptions;
    }


    private function _makeServiceToArgumentsMapByAnnotation(\ReflectionFunctionAbstract $reflectionMethod): array
    {
        if (! $reflectionMethod->isUserDefined())
            return [];

        /**
         * Parse method doc block to match against Service name to method name argument
         *
         * @param <type_hint> $<name> @Service <service_name>
         */
        $regex = '/(@param\s*)(?P<type_hint>[\w\\\|]+\s*|)(?P<argument_name>[$\w\|]+\s*)(@Service)\s*(?P<service_name>[\w\\/._-]+\s*|)/';

        $mapServices = [];
        if (preg_match_all($regex, $reflectionMethod->getDocComment(), $matches))
        {
            foreach ($matches['argument_name'] as $i => $argumentName) {
                $argumentName = ltrim(trim($argumentName), '$');
                $serviceName  = trim($matches['service_name'][$i]);

                $mapServices[$argumentName] = ['service_name' => $serviceName];
            }
        }

        return $mapServices;
    }

    private function _resolverWrapper(): aResolver
    {
        if ($this->_resolverWrapper)
            return $this->_resolverWrapper;


        if (is_callable($this->instantiable))
            return $this->_resolverWrapper = new CallableResolver($this->instantiable);

        return $this->_resolverWrapper = new InstantiatorResolver($this->instantiable);
    }
}
