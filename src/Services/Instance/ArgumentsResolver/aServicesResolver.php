<?php
namespace Poirot\ServiceManager\Services\Instance\ArgumentsResolver;

use Poirot\ServiceManager\Interfaces\iService;
use Poirot\ServiceManager\Interfaces\iServicesContainer;
use Poirot\ServiceManager\Interfaces\Pacts\iServicesProvider;
use Poirot\Std\ArgumentsResolver\aResolver;


abstract class aServicesResolver
    extends aResolver
    implements iServicesProvider
{
    /** @var iServicesContainer */
    private $services;


    /**
     * Constructor.
     *
     * @param iServicesContainer $container
     * @param array              $defaultOptions
     */
    function __construct(iServicesContainer $container, array $defaultOptions = [])
    {
        foreach ($defaultOptions as $key => $v) {
            if ($v instanceof iService)
                $defaultOptions[$key] = $container->make($v);
        }

        $this->services = $container;
        parent::__construct($defaultOptions);
    }

    // Implement Service Manager Aware:

    /**
     * @inheritDoc
     */
    function servicesContainer(): iServicesContainer
    {
        return $this->services;
    }
}
