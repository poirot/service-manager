<?php
namespace Poirot\ServiceManager\Services;

use Poirot\ServiceManager\Interfaces\iServicesContainer;
use Poirot\ServiceManager\Interfaces\Pacts\iServicesAware;
use Poirot\ServiceManager\Services\Instance\ArgumentsResolver\InstantiableServicesResolver;
use Poirot\Std\ArgumentsResolver;


class Instance
    extends aService
    implements iServicesAware
{
    /** @var mixed */
    protected $instantiable;
    /** @var array|null */
    protected $instanceParams = [];
    /** @var iServicesContainer */
    protected $sm;


    /**
     * Constructor
     *
     * @param mixed      $instantiable   Instantiable value it might be a method, class, or resolvable reflect
     * @param array|null $instanceParams Instance construct options if it's not resolved by Service manager
     * @param bool       $allowOverride
     * @param bool       $isSharable
     */
    function __construct($instantiable, array $instanceParams = [], bool $allowOverride = true, bool $isSharable = true)
    {
        $this->instantiable = $instantiable;
        $this->instanceParams = $instanceParams;
        parent::__construct($allowOverride, $isSharable);
    }

    /**
     * @inheritDoc
     */
    function createService()
    {
        $createdService = (new ArgumentsResolver(
            new InstantiableServicesResolver($this->instantiable, $this->sm, $this->instanceParams)
        ))->resolve();

        return $createdService;
    }

    /**
     * Set Service Manager
     *
     * @param iServicesContainer $sm
     */
    function setServicesContainer(iServicesContainer $sm)
    {
        $this->sm = $sm;
    }
}
