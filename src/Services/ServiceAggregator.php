<?php
namespace Poirot\ServiceManager\Services;

use Poirot\ServiceManager\Interfaces\iService;
use Poirot\ServiceManager\Container;


class ServiceAggregator
    extends aAggregateService
{
    /** @var Container */
    private $wrappedContainer;

    /**
     * Constructor
     *
     * @param Container $wrappedContainer Service manager
     * @param bool           $allowOverride
     */
    function __construct(Container $wrappedContainer, bool $allowOverride = true)
    {
        $this->wrappedContainer = $wrappedContainer;
        parent::__construct($allowOverride);
    }

    /**
     * @inheritDoc
     */
    function canCreateService(string $normalizedServiceName): bool
    {
        return $this->wrappedContainer->has($normalizedServiceName);
    }

    /**
     * @inheritDoc
     * @return iService
     * @throws \RuntimeException
     */
    function createService()
    {
        return $this->wrappedContainer->get($this->getName());
    }
}
