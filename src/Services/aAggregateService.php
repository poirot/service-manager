<?php
namespace Poirot\ServiceManager\Services;

use Poirot\ServiceManager\Interfaces\iService;
use Poirot\ServiceManager\Interfaces\Services\iFeatureServiceAggregate;
use Poirot\ServiceManager\Traits\tNormalizer;


abstract class aAggregateService
    extends aService
    implements iFeatureServiceAggregate
{
    use tNormalizer;

    /** @var string */
    protected $name;


    /**
     * @inheritDoc
     */
    abstract function canCreateService(string $normalizedServiceName): bool;


    /**
     * @inheritDoc
     */
    function withName(string $normalizedServiceName): iService
    {
        $new = clone $this;
        $new->name = $normalizedServiceName;
        return $new;
    }

    /**
     * @return string
     */
    function getName(): string
    {
        return $this->name;
    }
}
