<?php
namespace Poirot\ServiceManager\Services;

use Poirot\ServiceManager\Interfaces\iService;


abstract class aService
    implements iService
{
    /** @var bool */
    protected $allowOverride = true;
    /** @var bool */
    protected $isSharable = true;

    /**
     * @inheritdoc
     */
    abstract function createService();

    /**
     * Constructor
     *
     * @param bool $allowOverride
     * @param bool $isSharable
     */
    function __construct(bool $allowOverride = true, bool $isSharable = true)
    {
        $this->setAllowOverride($allowOverride);
        $this->setSharable($isSharable);
    }

    /**
     * Set is service allowed to override
     *
     * @param bool $canReplaced
     *
     * @return $this
     */
    function setAllowOverride(bool $canReplaced = true): self
    {
        $this->allowOverride = $canReplaced;
        return $this;
    }

    /**
     * @inheritDoc
     */
    function isAllowOverride(): bool
    {
        return $this->allowOverride;
    }

    /**
     * Set is service sharable?
     *
     * @param bool $sharable
     *
     * @return $this
     */
    function setSharable(bool $sharable = true): self
    {
        $this->isSharable = $sharable;
        return $this;
    }

    /**
     * @inheritDoc
     */
    function isSharable(): bool
    {
        return $this->isSharable;
    }
}
