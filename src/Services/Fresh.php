<?php
namespace Poirot\ServiceManager\Services;

use Poirot\ServiceManager\Interfaces\iServicesContainer;
use Poirot\ServiceManager\Interfaces\Pacts\iServicesAware;


class Fresh
 extends aService
    implements iServicesAware
{
    /** @var string */
    protected $serviceToRefresh;
    /** @var iServicesContainer */
    private $sm;


    /**
     * Constructor
     *
     * @param string $serviceToRefresh
     * @param bool   $allowOverride
     */
    function __construct(string $serviceToRefresh, bool $allowOverride = true)
    {
        $this->serviceToRefresh = $serviceToRefresh;
        parent::__construct($allowOverride, false);
    }

    /**
     * @inheritDoc
     */
    function createService()
    {
        return $this->sm->fresh($this->serviceToRefresh);
    }

    // ..

    /**
     * @inheritDoc
     */
    function setServicesContainer(iServicesContainer $sm)
    {
        $this->sm = $sm;
    }
}
