<?php
namespace Poirot\ServiceManager\Services;

use Poirot\ServiceManager\Interfaces\iServicesContainer;
use Poirot\ServiceManager\Interfaces\Pacts\iServicesAware;


class Get
 extends aService
 implements iServicesAware
{
    /** @var string */
    private $serviceToGet;
    /** @var iServicesContainer */
    private $sm;


    /**
     * Constructor
     *
     * @param string $serviceToGet
     * @param bool   $allowOverride
     */
    function __construct(string $serviceToGet, bool $allowOverride = true)
    {
        $this->serviceToGet = $serviceToGet;
        parent::__construct($allowOverride, true);
    }

    /**
     * @inheritDoc
     */
    function createService()
    {
        return $this->sm->get($this->serviceToGet);
    }

    // ..

    /**
     * @inheritDoc
     */
    function setServicesContainer(iServicesContainer $sm)
    {
        $this->sm = $sm;
    }
}
