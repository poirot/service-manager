<?php
namespace Poirot\ServiceManager\Services;

use Poirot\ServiceManager\Interfaces\iServicesContainer;
use Poirot\ServiceManager\Interfaces\Pacts\iServicesAware;


class Factory
    extends aService
    implements iServicesAware
{
    /** @var callable */
    protected $callable;
    /** @var iServicesContainer */
    protected $sm;


    /**
     * Constructor
     *
     * @param callable $serviceFactory Callable in F(FactoryService $service)
     * @param bool     $allowOverride
     * @param bool     $isSharable
     */
    function __construct(callable $serviceFactory, bool $allowOverride = true, bool $isSharable = true)
    {
        $this->callable = $serviceFactory;
        parent::__construct($allowOverride, $isSharable);
    }

    /**
     * @inheritDoc
     */
    function createService()
    {
        return call_user_func($this->callable, $this->sm);
    }

    /**
     * Set Service Manager
     *
     * @param iServicesContainer $sm
     */
    function setServicesContainer(iServicesContainer $sm)
    {
        $this->sm = $sm;
    }
}
