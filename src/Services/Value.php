<?php
namespace Poirot\ServiceManager\Services;


class Value
    extends aService
{
    protected $value;

    /**
     * Constructor
     *
     * @param mixed $value
     * @param bool  $allowOverride
     * @param bool  $isSharable
     */
    function __construct($value, bool $allowOverride = true, bool $isSharable = true)
    {
        $this->value = $value;
        parent::__construct($allowOverride, $isSharable);
    }

    /**
     * @inheritDoc
     */
    function createService()
    {
        return $this->value;
    }
}
