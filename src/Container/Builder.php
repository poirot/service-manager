<?php
namespace Poirot\ServiceManager\Container;

use Poirot\ServiceManager\Interfaces\Exceptions\iErrorDefineImplementation;
use Poirot\ServiceManager\Interfaces\iContainerListenerProvider;
use Poirot\ServiceManager\Interfaces\iService;
use Poirot\ServiceManager\Container;
use Poirot\ServiceManager\Interfaces\Listeners\iAfterRegistrationListener;
use Poirot\ServiceManager\Interfaces\Listeners\iBeforeRegistrationListener;
use Poirot\ServiceManager\Interfaces\Listeners\iContainerListener;
use Poirot\ServiceManager\Interfaces\Listeners\iInitializer;
use Poirot\ServiceManager\Services\Value;
use Poirot\Std\ArgumentsResolver;
use Poirot\Std\ArgumentsResolver\aResolver;
use Poirot\Std\Configurable\aConfigurableSetter;
use Poirot\Std\Configurable\Params;


class Builder
    extends aConfigurableSetter
{
    protected $implementations = [];
    protected $aliases = [];
    protected $attached_events = [];
    protected $services = [];


    /**
     * Configure service manager
     *
     * @param Container $sm
     *
     * @return Container
     * @throws iErrorDefineImplementation
     * @throws \TypeError
     */
    function build(Container $sm)
    {
        // Define Services Implementations and Aliases Should Happen First:
        $this->_buildImplementation($sm);
        $this->_buildAliases($sm);
        $this->_buildEvents($sm);
        $this->_buildService($sm);

        return $sm;
    }


    // Setter Options:

    /**
     * Set Service's Implementations
     *
     * 'implementations' => [
     *    'serviceName' => \stdClass::class,
     *    new Params([
     *       'service_name' => 'serviceName',
     *       'implement' => \stdClass::class
     *    ]),
     * ],
     *
     * @param array $implementations
     *
     * @return $this
     * @throws \Exception
     */
    protected function setImplementations(array $implementations): self
    {
        foreach ($implementations as $serviceName => $implementation) {
            if ($implementation instanceof Params)
                $this->_setConfigThroughMethod('_addImplementation', $implementation);
            else
                $this->_addImplementation($serviceName, $implementation);
        }

        return $this;
    }

    /**
     * Add new Service Defined Implementation
     *
     * @param string        $serviceName
     * @param string|object $implement   In case of string FCQN to valid interface or class
     *
     * @return $this
     */
    protected function _addImplementation(string $serviceName, $implement): self
    {
        $this->implementations[$serviceName] = $implement;
        return $this;
    }

    /**
     * Set Services Aliases
     *
     * 'aliases' => [
     *    'serviceName' => 'aliasName',
     *    new Params([
     *       'service_name' => 'serviceName',
     *       'aliases' => 'aliasNameOne',
     *    ])
     * ],
     *
     *
     * @param array $aliases
     *
     * @return $this
     * @throws \Exception
     */
    protected function setAliases(array $aliases): self
    {
        foreach ($aliases as $serviceName => $serviceAliases) {
            if ($serviceAliases instanceof Params)
                $this->_setConfigThroughMethod('_addAlias',  $serviceAliases);
            else
                $this->_addAlias($serviceName, ...(array) $serviceAliases);
        }

        return $this;
    }

    /**
     * Set Service Aliases
     *
     * @param string $serviceName
     * @param string ...$aliases
     *
     * @return $this
     */
    protected function _addAlias(string $serviceName, string ...$aliases): self
    {
        $this->aliases[$serviceName] = $aliases;
        return $this;
    }

    /**
     * Set Service Initializers
     *
     * 'attached_events' => [
     *    new InitializerFactory(function() {}),
     *    new Params([
     *       'listener' => new ArgumentsResolver\InstantiatorResolver(InitializerFactory::class, [
     *         function() {}
     *       ]),
     *       'priority' => 10,
     *   ])
     * ],
     *
     * @param array $events
     *
     * @return $this
     * @throws \Exception
     */
    protected function setAttachedEvents(array $events): self
    {
        foreach ($events as $listener) {
            if ($listener instanceof Params)
                $this->_setConfigThroughMethod('_addAttachedEventListener', $listener);
            else
                $this->_addAttachedEventListener($listener);
        }

        return $this;
    }

    /**
     * Set Event Listener
     *
     * @param iContainerListener|iContainerListenerProvider  $listener
     * @param null|int                                    $priority Priority
     *
     * @return $this
     */
    protected function _addAttachedEventListener($listener, $priority = null): self
    {
        $this->attached_events[] = [
            'listener' => $listener,
            'priority' => $priority,
        ];

        return $this;
    }

    /**
     * Set Services
     *
     * 'services' => [
     *    'service_name' => new Factory('ActualServiceName', function () {
     *       return 'Created-Service';
     *  }),
     *  'service_name' => new ArgumentsResolver\InstantiatorResolver(Instantiator::class, [
     *    'instantiable' => \stdClass::class
     *  ]),
     * ],
     *
     * @param array $services
     *
     * @return $this
     * @throws \Exception
     */
    protected function setServices(array $services): self
    {
        foreach ($services as $serviceName => $service) {
            if ($service instanceof Params)
                $this->_setConfigThroughMethod('_addService',  $service);
            else
                $this->_addService($serviceName, $service);
        }

        return $this;
    }

    /**
     * Set Service
     *
     * @param string             $serviceName
     * @param iService|aResolver $service
     *
     * @return $this
     */
    protected function _addService(string $serviceName, $service): self
    {
        $this->services[$serviceName] = $service;
        return $this;
    }

    // Build Service Manager with given configurations:

    /**
     * @param Container $sm
     * @throws iErrorDefineImplementation
     */
    protected function _buildImplementation(Container $sm): void
    {
        if (empty($this->implementations))
            return;

        foreach ($this->implementations as $serviceName => $interface)
            $sm->setImplementation($serviceName, $interface);
    }

    protected function _buildAliases(Container $sm)
    {
        foreach($this->aliases as $serviceName => $aliases)
            $sm->setAliases($serviceName, ...$aliases);
    }

    /**
     * @param Container $sm
     * @throws \TypeError
     */
    protected function _buildEvents(Container $sm)
    {
        if (empty($this->attached_events))
            return;

        foreach ($this->attached_events as $listenerConf) {
            $listener = $listenerConf['listener'];
            $priority = $listenerConf['priority'];

            if ($listener instanceof aResolver)
                $listener = (new ArgumentsResolver($listener))
                    ->resolve();

            if ($listener instanceof iContainerListenerProvider) {
                $listener->attachToEvents($sm->events());
            } else {
                if ($listener instanceof iBeforeRegistrationListener)
                    $sm->events()->onBeforeServiceRegistration($listener, $priority);

                if ($listener instanceof iAfterRegistrationListener)
                    $sm->events()->onServiceRegistration($listener, $priority);

                if ($listener instanceof iInitializer)
                    $sm->events()->onInitialize($listener, $priority);
            }
        }
    }

    /**
     * @param Container $sm
     */
    protected function _buildService(Container $sm)
    {
        if (empty($this->services))
            return;

        foreach ($this->services as $serviceName => $service) {
            if ($service instanceof aResolver)
                $service = (new ArgumentsResolver($service))
                    ->resolve();

            if (! $service instanceof iService)
                $service = new Value($service);

            $sm->set($serviceName, $service);
        }
    }
}
