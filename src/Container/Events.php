<?php
namespace Poirot\ServiceManager\Container;

use Poirot\ServiceManager\Container;
use Poirot\ServiceManager\Interfaces\iService;
use Poirot\ServiceManager\Interfaces\iServicesContainer;
use Poirot\ServiceManager\Interfaces\Listeners\iAfterRegistrationListener;
use Poirot\ServiceManager\Interfaces\Listeners\iBeforeRegistrationListener;
use Poirot\ServiceManager\Interfaces\Listeners\iInitializer;
use Poirot\ServiceManager\Interfaces\Pacts\iServicesAware;
use Poirot\Std\Exceptions\ObjectImmutableError;
use SplPriorityQueue;


class Events
    implements iServicesAware
{
    /** @var Container */
    protected $targetContainer;

    protected $eventListeners = [
        iInitializer::class => null,
        iBeforeRegistrationListener::class => null,
        iAfterRegistrationListener::class => null,
    ];


    /**
     * Attach Listener
     *
     * @param iBeforeRegistrationListener $listener
     * @param int|null                    $priority
     *
     * @return $this
     */
    function onBeforeServiceRegistration(iBeforeRegistrationListener $listener, ?int $priority = null): self
    {
        $this->_getPriorityQueue(iBeforeRegistrationListener::class)
            ->insert($listener, $priority ?? 10);

        return $this;
    }

    /**
     * Trigger Right Before Registering Service To Container
     *
     * @param string        $serviceName
     * @param iService      $service           Given service
     * @param iService|null $registeredService Previous service if it has something
     */
    function triggerBeforeServiceRegistration(string $serviceName, iService $service, ?iService $registeredService)
    {
        /** @var iBeforeRegistrationListener $listener */
        foreach (clone $this->_getPriorityQueue(iBeforeRegistrationListener::class) as $listener)
            $listener->__invoke($serviceName, $service, $registeredService, $this->targetContainer);
    }

    /**
     * Attach Listener
     *
     * @param iAfterRegistrationListener $listener
     * @param int|null                   $priority
     *
     * @return $this
     */
    function onServiceRegistration(iAfterRegistrationListener $listener, ?int $priority = null): self
    {
        $this->_getPriorityQueue(iAfterRegistrationListener::class)
            ->insert($listener, $priority ?? 10);

        return $this;
    }

    /**
     * Trigger Right After Registering Service To Container
     *
     * @param string        $serviceName
     * @param iService      $service           Given service
     */
    function triggerServiceRegistration(string $serviceName, iService $service)
    {
        /** @var iAfterRegistrationListener $listener */
        foreach (clone $this->_getPriorityQueue(iAfterRegistrationListener::class) as $listener)
            $listener->__invoke($serviceName, $service, $this->targetContainer);
    }

    /**
     * Attach Listener
     *
     * @param iInitializer $listener
     * @param null|int     $priority
     *
     * @return $this
     */
    function onInitialize(iInitializer $listener, ?int $priority = null): self
    {
        $this->_getPriorityQueue(iInitializer::class)
            ->insert($listener, $priority ?? 10);

        return $this;
    }

    /**
     * Initialize Service
     *
     * @param iService|mixed $instance Service or resulted created service
     *
     * @return void
     */
    function triggerInitialize($instance): void
    {
        /** @var iInitializer $listener */
        foreach (clone $this->_getPriorityQueue(iInitializer::class) as $listener)
            $listener->__invoke($instance, $this->targetContainer);
    }

    // Implement iServicesAware:

    /**
     * @inheritDoc
     * @throws ObjectImmutableError
     */
    function setServicesContainer(iServicesContainer $sm)
    {
        if ($this->targetContainer)
            throw new ObjectImmutableError('Event object already has an container set.');

        $this->targetContainer = $sm;
    }

    // ..

    protected function _getPriorityQueue(string $eventName): SplPriorityQueue
    {
        if (! isset($this->eventListeners[$eventName]))
            $this->eventListeners[$eventName] = new SplPriorityQueue;

        return $this->eventListeners[$eventName];
    }
}
