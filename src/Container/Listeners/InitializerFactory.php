<?php
namespace Poirot\ServiceManager\Container\Listeners;

use Poirot\ServiceManager\Container;
use Poirot\ServiceManager\Interfaces\Listeners\iInitializer;


class InitializerFactory
    implements iInitializer
{
    /** @var callable */
    protected $callable;


    /**
     * Constructor
     *
     * @param callable $callableFactory Initializer factory function($instance, $sm)
     */
    function __construct(callable $callableFactory)
    {
        $this->callable = $callableFactory;
    }

    /**
     * @inheritDoc
     */
    function __invoke($instance, Container $container): void
    {
        call_user_func($this->callable, $instance, $container);
    }
}
