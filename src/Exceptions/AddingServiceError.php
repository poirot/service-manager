<?php
namespace Poirot\ServiceManager\Exceptions;

use Poirot\ServiceManager\Interfaces\Exceptions\iErrorAddingService;

class AddingServiceError
    extends \RuntimeException
    implements iErrorAddingService
{ }
