<?php
namespace Poirot\ServiceManager\Exceptions;

use Poirot\ServiceManager\Interfaces\Exceptions\iErrorAddingService;

class AddingServiceInvalidArgumentsError
    extends \InvalidArgumentException
    implements iErrorAddingService
{ }
