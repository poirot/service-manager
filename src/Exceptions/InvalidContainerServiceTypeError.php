<?php
namespace Poirot\ServiceManager\Container\Exception;

class InvalidContainerServiceTypeError
    extends \RuntimeException
{ }
