<?php
namespace Poirot\ServiceManager\Exceptions;

use Poirot\ServiceManager\Interfaces\Exceptions\iErrorCreateService;

class CreateServiceError
    extends \RuntimeException
    implements iErrorCreateService
{

}
