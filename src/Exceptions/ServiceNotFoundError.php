<?php
namespace Poirot\ServiceManager\Exceptions;

use Poirot\ServiceManager\Interfaces\Exceptions\iErrorServiceNotFound;

class ServiceNotFoundError
    extends \RuntimeException
    implements iErrorServiceNotFound
{ }
