<?php
namespace Poirot\ServiceManager\Exceptions;

use Poirot\ServiceManager\Interfaces\Exceptions\iErrorDefineImplementation;

class DefineImplementationInvalidArgumentError
    extends \InvalidArgumentException
    implements iErrorDefineImplementation
{

}
