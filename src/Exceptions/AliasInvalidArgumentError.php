<?php
namespace Poirot\ServiceManager\Exceptions;

use Poirot\ServiceManager\Interfaces\Exceptions\iErrorAlias;

class AliasInvalidArgumentError
    extends \InvalidArgumentException
    implements iErrorAlias
{

}
