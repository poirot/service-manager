<?php
namespace Poirot\ServiceManager\Exceptions;

use Poirot\ServiceManager\Interfaces\Exceptions\iErrorAddingService;
use Poirot\ServiceManager\Interfaces\Exceptions\iErrorAlias;
use Poirot\ServiceManager\Interfaces\Exceptions\iErrorCreateService;
use Poirot\ServiceManager\Interfaces\Exceptions\iErrorDefineImplementation;
use Poirot\ServiceManager\Interfaces\Exceptions\iErrorServiceNotFound;
use Poirot\Std\Exceptions\aExceptionsFacade;


class AbstractExceptionsFacade
    extends aExceptionsFacade
{
    static function getAliasError(array $data, int $errorCode): iErrorAlias
    {
        if ($errorCode === AliasInvalidArgumentError::ErrorCode_AliasAndServiceNameAreSame) {
            $errorMessage = self::getProcessedErrMessage(
                'Alias name (%alias_name%) couldn`t be similar to service name.',
                $data
            );

            return new AliasInvalidArgumentError($errorMessage, $errorCode);
        }

        if ($errorCode === AliasInvalidArgumentError::ErrorCode_InvalidAliasValue) {
            $errorMessage = self::getProcessedErrMessage(
                'Invalid Alias or Service Name is Given.',
                $data
            );

            return new AliasInvalidArgumentError($errorMessage, $errorCode);
        }

        if ($errorCode === CyclingDefinedAliasError::ErrorCode_CyclingDefinedAliasesFound) {
            $errorMessage = self::getProcessedErrMessage(
                'Cycles were detected within the provided aliases: %aliases%',
                $data
            );

            return new CyclingDefinedAliasError($errorMessage, $errorCode);
        }


        $errorMessage = 'Unknown error!';
        return new AliasInvalidArgumentError($errorMessage, $errorCode);
    }

    static function getImplementationDefineError(array $data, int $errorCode): iErrorDefineImplementation
    {
        if ($errorCode === DefineImplementationInvalidArgumentError::ErrorCode_InvalidImplementationValue) {
            $errorMessage = self::getProcessedErrMessage('Define Implementation for service (%service_name%) failed, '
                . 'Implement argument must be valid interface name; given (%implementation%).', $data);

            return new DefineImplementationInvalidArgumentError($errorMessage, $errorCode);
        }

        if ($errorCode === DefineImplementationInvalidArgumentError::ErrorCode_DefinedImplementationCantGetChange) {
            $errorMessage = self::getProcessedErrMessage('Implementation %implementation% is already defined for service (%service_name%), '
                . 'new implementation %new_implementation% should be extending current implementation class or interface.', $data);

            return new DefineImplementationInvalidArgumentError($errorMessage, $errorCode);
        }


        if ($errorCode === DefineImplementationError::ErrorCode_ServiceAlreadyDefined) {
            $errorMessage =  self::getProcessedErrMessage('Service (%service_name%) is already exists;'
                . 'Interface must define before service registration.', $data);
        } else {
            $errorMessage = 'Unknown error!';
        }

        return new DefineImplementationError($errorMessage, $errorCode);
    }

    static function getCreateServiceError(array $data, int $errorCode, \Exception $cause = null): iErrorCreateService
    {
        if ($errorCode === CreateServiceError::ErrorCode_ServiceCreateError) {
            $errorMessage = self::getProcessedErrMessage(
                'An exception was raised while creating (%service_name%); no instance returned.',
                $data);
        } elseif ($errorCode === CreateServiceError::ErrorCode_ServiceCreatedNothing) {
            $errorMessage = self::getProcessedErrMessage(
                'Service (%service_name%) meanwhile found but returned null.',
                $data);
        } elseif ($errorCode === CreateServiceError::ErrorCode_ServiceInvalidImplementation) {
            $errorMessage = self::getProcessedErrMessage(
                'Invalid implementation for service (%service_name%); Object (%created_service%) must implement (%implementation%).',
                $data);
        } else {
            $errorMessage = 'Unknown error!';
        }

        return new CreateServiceError($errorMessage, $errorCode, $cause);
    }

    static function getAddingServiceError(array $data, int $errorCode): iErrorAddingService
    {
        if ($errorCode === AddingServiceInvalidArgumentsError::ErrorCode_ServiceNameCantBeEmpty) {
            return new AddingServiceInvalidArgumentsError(
                self::getProcessedErrMessage('Service (%s) has an empty name.', $data),
                $errorCode);
        }


        if ($errorCode === AddingServiceError::ErrorCode_ReplacingServiceNotAllowed)
            $errorMessage = self::getProcessedErrMessage(
                'A service by the name or alias (%service_name%) already exists and not allowed be replaced.'
                , $data);
        else
            $errorMessage = 'Unknown error!';

        return new AddingServiceError($errorMessage, $errorCode);
    }

    static function getServiceNotFoundError(array $data): iErrorServiceNotFound
    {
        return new ServiceNotFoundError(
            self::getProcessedErrMessage('Service by the name or alias (%service_name%) not found.', $data)
        );
    }
}
