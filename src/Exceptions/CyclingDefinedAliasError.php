<?php
namespace Poirot\ServiceManager\Exceptions;

use Poirot\ServiceManager\Interfaces\Exceptions\iErrorAlias;

class CyclingDefinedAliasError
    extends \RuntimeException
    implements iErrorAlias
{

}
