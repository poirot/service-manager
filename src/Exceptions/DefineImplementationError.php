<?php
namespace Poirot\ServiceManager\Exceptions;

use Poirot\ServiceManager\Interfaces\Exceptions\iErrorDefineImplementation;

class DefineImplementationError
    extends \RuntimeException
    implements iErrorDefineImplementation
{

}
