<?php
namespace Poirot\ServiceManager\Interfaces;

use Poirot\ServiceManager\Container\Events;

interface iContainerListenerProvider
{
    /**
     * Attach Listeners To Container Events
     *
     * @param Events $containerEvents
     */
    function attachToEvents(Events $containerEvents): void;
}
