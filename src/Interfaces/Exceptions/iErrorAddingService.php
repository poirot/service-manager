<?php
namespace Poirot\ServiceManager\Interfaces\Exceptions;

interface iErrorAddingService
    extends iErrorServiceManager
{
    const ErrorCode_ReplacingServiceNotAllowed = 2;
    const ErrorCode_ServiceNameCantBeEmpty = 4;
}
