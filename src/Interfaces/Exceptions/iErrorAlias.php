<?php
namespace Poirot\ServiceManager\Interfaces\Exceptions;

interface iErrorAlias
    extends iErrorServiceManager
{
    const ErrorCode_InvalidAliasValue = 2;
    const ErrorCode_AliasAndServiceNameAreSame = 4;
    const ErrorCode_CyclingDefinedAliasesFound = 6;
}
