<?php
namespace Poirot\ServiceManager\Interfaces\Exceptions;

interface iErrorServiceNotFound
    extends iErrorServiceManager
{ }
