<?php
namespace Poirot\ServiceManager\Interfaces\Exceptions;

interface iErrorDefineImplementation
    extends iErrorServiceManager
{
    const ErrorCode_InvalidImplementationValue = 2;
    const ErrorCode_ServiceAlreadyDefined = 4;
    const ErrorCode_DefinedImplementationCantGetChange = 6;
}
