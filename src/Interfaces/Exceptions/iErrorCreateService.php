<?php
namespace Poirot\ServiceManager\Interfaces\Exceptions;

interface iErrorCreateService
    extends iErrorServiceManager
{
    const ErrorCode_ServiceCreateError = 2;
    const ErrorCode_ServiceCreatedNothing = 4;
    const ErrorCode_ServiceInvalidImplementation = 8;
}
