<?php
namespace Poirot\ServiceManager\Interfaces;

use Poirot\ServiceManager\Interfaces\Exceptions\iErrorCreateService;
use Poirot\ServiceManager\Interfaces\Exceptions\iErrorServiceNotFound;


interface iServicesContainer
{
    /**
     * Returns true if the container can return an entry for the given identifier.
     * Returns false otherwise.
     *
     * @param string $serviceName Service name or alias
     *
     * @return bool
     */
    function has(string $serviceName): bool;

    /**
     * Finds an entry of the container by its identifier and returns it
     *
     * @param string $serviceName Service name or alias
     *
     * @return mixed
     * @throws iErrorCreateService Error while retrieving the entry
     * @throws iErrorServiceNotFound No service was found
     */
    function get(string $serviceName);

    /**
     * Retrieve a fresh instance of service
     *
     * @param string $serviceName Service name or alias
     *
     * @return mixed
     * @throws iErrorCreateService Error while retrieving the entry
     * @throws iErrorServiceNotFound No service was found
     */
    function fresh(string $serviceName);

    /**
     * Create an instance by current service manager configuration to resolve
     * dependencies or initialize objects and more ...
     *
     * @param iService $service
     *
     * @return mixed
     * @throws iErrorCreateService Error while creating the entry
     */
    function make(iService $service);
}
