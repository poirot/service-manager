<?php
namespace Poirot\ServiceManager\Interfaces\Listeners;

use Poirot\ServiceManager\Container;
use Poirot\ServiceManager\Interfaces\iService;


interface iAfterRegistrationListener
    extends iContainerListener
{
    /**
     * Trigger Before Registering Service To Container
     *
     * @param string        $serviceName
     * @param iService      $service           Given service
     * @param Container     $container
     *
     * @return void
     */
    function __invoke(string $serviceName, iService $service, Container $container): void;
}
