<?php
namespace Poirot\ServiceManager\Interfaces\Listeners;

use Poirot\ServiceManager\Container;
use Poirot\ServiceManager\Interfaces\iService;


interface iBeforeRegistrationListener
    extends iContainerListener
{
    /**
     * Trigger Before Registering Service To Container
     *
     * @param string        $serviceName
     * @param iService      $service           Given service
     * @param iService|null $registeredService Previous service if it has something
     * @param Container     $container
     *
     * @return void
     */
    function __invoke(string $serviceName, iService $service, ?iService $registeredService, Container $container): void;
}
