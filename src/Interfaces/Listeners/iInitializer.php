<?php
namespace Poirot\ServiceManager\Interfaces\Listeners;

use Poirot\ServiceManager\Container;


interface iInitializer
    extends iContainerListener
{
    /**
     * Initialize Service
     *
     * @param mixed     $instance
     * @param Container $container
     *
     * @return void
     */
    function __invoke($instance, Container $container): void;
}
