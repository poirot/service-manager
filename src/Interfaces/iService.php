<?php
namespace Poirot\ServiceManager\Interfaces;

/**
 * Service Which Can Register To Container
 *
 * services can extends these features interfaces in case they need implement extra feature:
 * @see iFeatureServiceAggregate
 */
interface iService
{
    /**
     * Create Expected Service
     *
     * @return mixed
     */
    function createService();

    // options:

    /**
     * The service which get cached and on each request
     * will not be created
     *
     * @return bool
     */
    function isSharable(): bool;

    /**
     * Check whether service can be replaced by another
     * service with same name in container?
     *
     * @return bool
     */
    function isAllowOverride(): bool;
}
