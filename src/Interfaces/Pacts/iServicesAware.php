<?php
namespace Poirot\ServiceManager\Interfaces\Pacts;

use Poirot\ServiceManager\Interfaces\iServicesContainer;

/**
 * Interface iCServiceAware
 *
 * - Classes that implement this interface
 *   can have parent Service Container injected
 *
 */
interface iServicesAware
{
    /**
     * Set Service Manager
     *
     * @param iServicesContainer $sm
     */
    function setServicesContainer(iServicesContainer $sm);
}
