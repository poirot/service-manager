<?php
namespace Poirot\ServiceManager\Interfaces\Pacts;

interface iServicesComplex
    extends iServicesAware
    , iServicesProvider
{ }
