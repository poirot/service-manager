<?php
namespace Poirot\ServiceManager\Interfaces\Pacts;

use Poirot\ServiceManager\Interfaces\iServicesContainer;

interface iServicesProvider
{
    /**
     * Service Manager
     *
     * @return iServicesContainer
     */
    function servicesContainer(): iServicesContainer;
}
