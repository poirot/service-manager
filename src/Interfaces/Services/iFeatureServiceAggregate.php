<?php
namespace Poirot\ServiceManager\Interfaces\Services;

use Poirot\ServiceManager\Interfaces\iService;


interface iFeatureServiceAggregate
    extends iService
{
    /**
     * Determine Which Can Create Service With Given Name?
     * 
     * @param string $normalizedServiceName Service name
     * 
     * @return bool
     */
    function canCreateService(string $normalizedServiceName): bool;

    /**
     * Set Service Name To Create
     *
     * @param string $normalizedServiceName
     * 
     * @return iFeatureServiceAggregate New clone of object
     */
    function withName(string $normalizedServiceName): iService;
}
