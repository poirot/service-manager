<?php
namespace Poirot\ServiceManager\Interfaces\Services;

use Poirot\ServiceManager\Interfaces\iContainerListenerProvider;
use Poirot\ServiceManager\Interfaces\iService;


interface iFeatureServiceEvent
    extends iService
    , iContainerListenerProvider
{

}
