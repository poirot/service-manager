<?php
namespace PoirotTest\ServiceManager\TestBuilder;

use PHPUnit\Framework\TestCase;
use Poirot\ServiceManager\Container\Events;
use Poirot\ServiceManager\Interfaces\iService;
use Poirot\ServiceManager\Container;
use Poirot\ServiceManager\Container\Builder;
use Poirot\ServiceManager\Interfaces\Listeners\iAfterRegistrationListener;
use Poirot\ServiceManager\Interfaces\Listeners\iBeforeRegistrationListener;
use Poirot\ServiceManager\Interfaces\Listeners\iInitializer;
use Poirot\Std\ArgumentsResolver\InstantiatorResolver;
use Poirot\Std\Configurable\Params;
use PoirotTest\ServiceManager\Dummy\DummyInitializer;
use PoirotTest\ServiceManager\Dummy\DummyService;


class ContainerBuilderTest extends TestCase
{
    /**
     * @dataProvider provideImplementationConfiguration
     */
    function testCanBuildImplementationByConfiguration($configuration)
    {
        $builder = $this->createBuilder()
            ->setConfigs($configuration);

        $servicesContainer = new Container;
        $this->assertEmpty($servicesContainer->hasImplementation('serviceName'));
        $builder->build($servicesContainer);
        $this->assertEquals(\stdClass::class, $servicesContainer->hasImplementation('serviceName'));
    }

    /**
     * @depends testCanBuildImplementationByConfiguration
     */
    function testBuilderShouldReturnSameObjectAsItGet()
    {
        $builder = $this->createBuilder()
            ->setConfigs([
                'implementations' => [
                    new Params([
                        'service_name' => 'serviceName',
                        'implement' => \stdClass::class
                    ])
                ]
            ]);

        $servicesContainer = new Container;
        $servicesContainerBuild = $builder->build($servicesContainer);
        $this->assertSame($servicesContainer, $servicesContainerBuild);
    }

    /**
     * @dataProvider provideAliasConfiguration
     */
    function testCanBuildAliasesByConfiguration($configuration)
    {
        $servicesContainer = new Container;
        $servicesContainer->set('serviceName', $this->getMockedServiceObject());

        $builder = $this->createBuilder()
            ->setConfigs($configuration);

        $this->assertFalse($servicesContainer->has('aliasName'));
        $builder->build($servicesContainer);
        $this->assertTrue($servicesContainer->has('aliasName'));
    }

    /**
     * @dataProvider provideEventListeners
     */
    function testCanBuildInitializer($listener, $registerMethod)
    {
        $mockedInitializer = $this->getMockBuilder($listener)->getMock();

        $mockedEventsAggregate = $this->getMockBuilder(Events::class)->getMock();
        $mockedEventsAggregate->expects($this->once())->method('setServicesContainer');
        $mockedEventsAggregate->expects($this->exactly(2))
            ->method($registerMethod)
            ->withConsecutive([$mockedInitializer, null], [$mockedInitializer, 10]);

        $builder = $this->createBuilder()
            ->setConfigs([
                'attached_events' => [
                    $mockedInitializer,
                    new Params([
                        'initializer' => $mockedInitializer,
                        'priority' => 10,
                    ])
                ],
            ]);


        $servicesContainer = new Container(null, $mockedEventsAggregate);
        $builder->build($servicesContainer);
        $this->assertSame($mockedEventsAggregate, $servicesContainer->events());
    }

    /**
     * @depends testCanBuildInitializer
     */
    function testCanResolveInstantiableInitializer()
    {
        $mockedEventsAggregate = $this->getMockBuilder(Events::class)->getMock();
        $mockedEventsAggregate->expects($this->once())
            ->method('onInitialize')
            ->with($this->isInstanceOf(DummyInitializer::class), null);

        $builder = $this->createBuilder()
            ->setConfigs([
                'attached_events' => [
                    new InstantiatorResolver(DummyInitializer::class),
                ]
            ]);

        $sm = new Container(null, $mockedEventsAggregate);
        $builder->build($sm);
    }

    function testCanBuildService()
    {
        $createdService = new \stdClass;

        $builder = $this->createBuilder()
            ->setConfigs([
                'services' => [
                    'serviceName' => new DummyService($createdService)
                ]
            ]);

        $sm = new Container;
        $builder->build($sm);
        $this->assertSame($createdService, $sm->get('serviceName'));
    }

    function testCanResolveInstantiableService()
    {
        $createdService = new \stdClass;

        $builder = $this->createBuilder()
            ->setConfigs([
                'services' => [
                    'serviceName' => new InstantiatorResolver(DummyService::class, [$createdService])
                ]
            ]);

        $sm = new Container;
        $builder->build($sm);
        $this->assertSame($createdService, $sm->get('serviceName'));
    }

    // ..

    function getMockedServiceObject($createdService = null)
    {
        /** @var iService $mockedService */
        $mockedService = $this->getMockBuilder(iService::class)->getMock();
        $mockedService->method('isSharable')->willReturn(true);

        if (null !== $createdService) {
            $mockedService->method('createService')
                ->willReturnCallback(function() use ($createdService) {
                    return $createdService;
                });
        }

        return $mockedService;
    }

    function createBuilder(): Builder
    {
        return new Builder;
    }

    function provideImplementationConfiguration()
    {
        return [
            ['configuration' => [
                'implementations' => [
                    'serviceName' => \stdClass::class,
                ],
            ]],
            ['configuration' => [
                'implementations' => [
                    new Params([
                        'service_name' => 'serviceName',
                        'implement' => \stdClass::class
                    ])
                ],
            ]],
        ];
    }

    function provideAliasConfiguration()
    {
        return [
            ['configuration' => [
                'aliases' => [
                    'serviceName' => 'aliasName',
                ],
            ]],
            ['configuration' => [
                'aliases' => [
                    new Params([
                        'service_name' => 'serviceName',
                        'aliases' => 'aliasName',
                    ])
                ],
            ]],
        ];
    }

    function provideEventListeners()
    {
        return [
            [iInitializer::class, 'onInitialize'],
            [iAfterRegistrationListener::class, 'onServiceRegistration'],
            [iBeforeRegistrationListener::class, 'onBeforeServiceRegistration'],
        ];
    }
}
