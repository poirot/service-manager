<?php
namespace PoirotTest\ServiceManager\Dummy;

use Poirot\ServiceManager\Container;
use Poirot\ServiceManager\Interfaces\Listeners\iInitializer;

class DummyInitializer
    implements iInitializer
{
    /**
     * @inheritDoc
     */
    function __invoke($instance, Container $container): void
    {
        // Implement initialize() method.
    }
}
