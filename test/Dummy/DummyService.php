<?php
namespace PoirotTest\ServiceManager\Dummy;

use Poirot\ServiceManager\Interfaces\iService;


class DummyService
    implements iService
{
    protected $createdService;


    function __construct($serviceToCreate)
    {
        $this->createdService = $serviceToCreate;
    }

    /**
     * @inheritDoc
     */
    function createService()
    {
        return $this->createdService;
    }

    /**
     * @inheritDoc
     */
    function isSharable(): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    function isAllowOverride(): bool
    {
        return true;
    }
}
