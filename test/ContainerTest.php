<?php
namespace PoirotTest\ServiceManager;

use PHPUnit\Framework\TestCase;
use Poirot\ServiceManager\Container\Events;
use Poirot\ServiceManager\Exceptions\AddingServiceError;
use Poirot\ServiceManager\Exceptions\AddingServiceInvalidArgumentsError;
use Poirot\ServiceManager\Exceptions\AliasInvalidArgumentError;
use Poirot\ServiceManager\Exceptions\CreateServiceError;
use Poirot\ServiceManager\Exceptions\CyclingDefinedAliasError;
use Poirot\ServiceManager\Exceptions\DefineImplementationError;
use Poirot\ServiceManager\Exceptions\DefineImplementationInvalidArgumentError;
use Poirot\ServiceManager\Interfaces\iService;
use Poirot\ServiceManager\Interfaces\iServicesContainer;
use Poirot\ServiceManager\Interfaces\Pacts\iServicesAware;
use Poirot\ServiceManager\Interfaces\Services\iFeatureServiceAggregate;
use Poirot\ServiceManager\Container;
use Poirot\ServiceManager\Interfaces\Services\iFeatureServiceEvent;
use Poirot\ServiceManager\Traits\tNormalizer;


class ContainerTest extends TestCase
{
    use tNormalizer;

    // Set Service

    /**
     * @dataProvider provideDifferentServiceInterfaces
     */
    function testServicesCanBeRegisteredSuccessfully($serviceInterface)
    {
        /** @var iService $mockedService */
        $mockedService = $this->getMockBuilder($serviceInterface)->getMock();

        $sm = $this->getServicesContainer();
        $sm->set('serviceName', $mockedService);
        $this->addToAssertionCount(1); // successful
    }

    function testThrowExceptionWhenServiceNameIsEmpty()
    {
        $this->expectException(AddingServiceInvalidArgumentsError::class);
        $this->expectExceptionCode(AddingServiceError::ErrorCode_ServiceNameCantBeEmpty);

        /** @var iService $mockedService */
        $mockedService = $this->getMockBuilder(iService::class)->getMock();

        $sm = $this->getServicesContainer();
        $sm->set('', $mockedService);
    }

    function testThrowExceptionWhenServiceNotAllowedOverride()
    {
        $this->expectException(AddingServiceError::class);
        $this->expectExceptionCode(AddingServiceError::ErrorCode_ReplacingServiceNotAllowed);

        /** @var iService $mockedService */
        $mockedService = $this->getMockBuilder(iService::class)->getMock();
        $mockedService->expects($this->once())->method('isAllowOverride')->willReturn(false);

        $sm = $this->getServicesContainer();
        $sm->set('serviceName', $mockedService);
        $sm->set('serviceName', $mockedService);
    }

    // Has Service Registered

    /**
     * @depends testServicesCanBeRegisteredSuccessfully
     */
    function testHasReturnsTrueIfServiceRegistered()
    {
        $serviceName = 'serviceName';

        /** @var iService $mockedService */
        $mockedService = $this->getMockBuilder(iService::class)->getMock();

        $sm = $this->getServicesContainer();
        $sm->set($serviceName, $mockedService);
        $this->assertTrue($sm->has($serviceName));

        // Service names are case insensitive
        $this->assertTrue($sm->has(strtolower($serviceName)));
    }

    /**
     * @depends testServicesCanBeRegisteredSuccessfully
     */
    function testHasReturnsTrueIfServiceAvailableThroughAggregateServices()
    {
        $serviceName = 'serviceName';

        /** @var iFeatureServiceAggregate $mockedService */
        $mockedService = $this->getMockBuilder(iFeatureServiceAggregate::class)->getMock();
        // result of has() method from aggregated services will not cached, each time canCreateService will be called
        $mockedService->expects($this->exactly(2))->method('canCreateService')
            ->with($this->_normalizeName($serviceName))->willReturn(true);

        $sm = $this->getServicesContainer();
        $sm->set('AggregateServiceProvider', $mockedService);
        $this->assertTrue($sm->has($serviceName));

        // Service names should be case insensitive
        $this->assertTrue($sm->has(strtolower($serviceName)));
    }

    // Create Service

    function testServiceAtMinimalRequirementCanBeCreatedSuccessfully()
    {
        $createdService = new \stdClass;

        /** @var iService $mockedService */
        $mockedService = $this->getMockBuilder(iService::class)->getMock();
        $mockedService->expects($this->once())->method('createService')->willReturn($createdService);

        $sm = $this->getServicesContainer();
        $this->assertSame($createdService, $sm->make($mockedService));;
    }

    function testCreatingGoesRecursiveWhenCreatedServiceIsInstanceOfServiceObject()
    {
        /** @var iService $secondMockedService */
        $secondMockedService = $this->getMockBuilder(iService::class)->getMock();
        $secondMockedService->expects($this->once())->method('createService')->willReturn(new \stdClass);

        /** @var iService $mockedService */
        $mockedService = $this->getMockBuilder(iService::class)->getMock();
        $mockedService->expects($this->once())->method('createService')->willReturn($secondMockedService);

        $sm = $this->getServicesContainer();
        $this->assertInstanceOf(\stdClass::class, $sm->make($mockedService));
    }

    function testServiceImplementedServicesAwareWillReceiveItOnInitialize()
    {
        $sm = $this->getServicesContainer();

        /** @var iService $mockedService */
        $mockedService = $this->getMockBuilder([iService::class, iServicesAware::class])->getMock();
        // Service instance iServicesAware will receive it
        $mockedService->expects($this->exactly(1))->method('setServicesContainer')->with($sm);
        $mockedService->expects($this->once())->method('createService')->willReturn('Service Test Created.');

        $sm->make($mockedService);
    }

    function testCreatedServiceImplementedServicesAwareWillReceiveItOnInitialize()
    {
        $sm = $this->getServicesContainer();

        $createdService = $this->getMockBuilder(iServicesAware::class)->getMock();;
        // Created instance of iServicesAware will receive it
        $createdService->expects($this->exactly(1))->method('setServicesContainer')->with($sm);

        /** @var iService $mockedService */
        $mockedService = $this->getMockBuilder(iService::class)->getMock();
        $mockedService->expects($this->once())->method('createService')->willReturn($createdService);

        $sm->make($mockedService);
    }

    function testCreatedServicesAreNotSame()
    {
        $sm = $this->getServicesContainer();

        /** @var iService $mockedService */
        $mockedService = $this->getMockBuilder(iService::class)->getMock();
        $mockedService->method('createService')->willReturnCallback(function() {
            return new \stdClass;
        });

        $this->assertNotSame(
            $sm->make($mockedService),
            $sm->make($mockedService)
        );
    }

    function testThrowExpectedExceptionWhenServiceCreationFailed()
    {
        $this->expectException(CreateServiceError::class);
        $this->expectExceptionCode(CreateServiceError::ErrorCode_ServiceCreateError);

        /** @var iService $mockedService */
        $mockedService = $this->getMockBuilder(iService::class)->getMock();
        $mockedService->expects($this->once())->method('createService')->willThrowException(new \RuntimeException);

        $this->getServicesContainer()
            ->make($mockedService);
    }

    function testThrowExceptionWhenServiceCreateNullValue()
    {
        $this->expectException(CreateServiceError::class);
        $this->expectExceptionCode(CreateServiceError::ErrorCode_ServiceCreatedNothing);

        /** @var iService $mockedService */
        $mockedService = $this->getMockBuilder(iService::class)->getMock();
        $mockedService->expects($this->once())->method('createService')->willReturn(null);

        $this->getServicesContainer()
            ->make($mockedService);
    }

    // Retrieve Fresh Service

    /**
     * @depends testServicesCanBeRegisteredSuccessfully
     * @depends testServiceAtMinimalRequirementCanBeCreatedSuccessfully
     */
    function testCanRetrieveFreshService()
    {
        $serviceName = 'serviceName';

        /** @var iService $mockedService */
        $mockedService = $this->getMockBuilder(iService::class)->getMock();
        $mockedService->expects($this->once())->method('createService')->willReturn(new \stdClass);

        $sm = $this->getServicesContainer();
        $sm->set($serviceName, $mockedService);

        $firstCreatedClass = $sm->fresh($serviceName);

        $this->assertInstanceOf(\stdClass::class, $firstCreatedClass);
    }

    /**
     * @depends testServicesCanBeRegisteredSuccessfully
     * @depends testServiceAtMinimalRequirementCanBeCreatedSuccessfully
     */
    function testFreshServicesAreNotSame()
    {
        $serviceName = 'serviceName';

        /** @var iService $mockedService */
        $mockedService = $this->getMockBuilder(iService::class)->getMock();
        $mockedService->expects($this->exactly(2))->method('createService')->willReturnCallback(function() {
            return new \stdClass;
        });

        $sm = $this->getServicesContainer();
        $sm->set($serviceName, $mockedService);

        $firstCreatedClass = $sm->fresh($serviceName);
        $secondCreatedClass = $sm->fresh($serviceName);

        $this->assertInstanceOf(\stdClass::class, $firstCreatedClass);
        $this->assertInstanceOf(\stdClass::class, $secondCreatedClass);

        $this->assertNotSame($firstCreatedClass, $secondCreatedClass);
    }

    /**
     * @depends testServicesCanBeRegisteredSuccessfully
     * @depends testServiceAtMinimalRequirementCanBeCreatedSuccessfully
     */
    function testWhenServiceNotRegisteredWillGoThroughRegisteredAggregateServices()
    {
        $serviceName = 'serviceName';

        /** @var iFeatureServiceAggregate $mockedService */
        $mockedService = $this->getMockBuilder(iFeatureServiceAggregate::class)->getMock();
        // Service will get cloned and name will be changed to the service which is required to be created
        $mockedService->expects($this->once())->method('createService')->willReturn(new \stdClass);
        $mockedService->expects($this->once())->method('canCreateService')->with($this->_normalizeName($serviceName))->willReturn(true);
        $mockedService->expects($this->once())->method('withName')->with($this->_normalizeName($serviceName))
            ->willReturn($mockedService);

        $sm = $this->getServicesContainer();
        $sm->set($serviceName, $mockedService);
        $this->assertInstanceOf(\stdClass::class, $sm->fresh($serviceName));
    }

    // Get Services

    function testContainerItselfHasAvailableAsService()
    {
        $sm = $this->getServicesContainer();
        $this->assertSame($sm, $sm->get(iServicesContainer::class));
    }

    function testServicesContainerInterfaceNotAllowedToReplace()
    {
        $mockedService = $this->getMockBuilder(iService::class)->getMock();

        $this->expectException(AddingServiceError::class);
        $this->expectExceptionCode(AddingServiceError::ErrorCode_ReplacingServiceNotAllowed);
        $sm = $this->getServicesContainer();
        $sm->set(iServicesContainer::class, $mockedService);
    }

    /**
     * @depends testCanRetrieveFreshService
     */
    function testGetServiceInstancesAreSame()
    {
        $serviceName = 'serviceName';

        /** @var iService $mockedService */
        $mockedService = $this->getMockBuilder(iService::class)->getMock();
        $mockedService->expects($this->once())->method('isSharable')->willReturn(true);
        $mockedService->expects($this->once())->method('createService')->willReturnCallback(function() {
            return new \stdClass;
        });

        $sm = $this->getServicesContainer();
        $sm->set($serviceName, $mockedService);

        $firstCreatedClass = $sm->get($serviceName);
        $secondCreatedClass = $sm->get($serviceName);

        $this->assertInstanceOf(\stdClass::class, $firstCreatedClass);
        $this->assertInstanceOf(\stdClass::class, $secondCreatedClass);

        $this->assertSame($firstCreatedClass, $secondCreatedClass);
    }

    /**
     * @depends testCanRetrieveFreshService
     */
    function testGetNotSharableServiceInstancesAreNotSame()
    {
        $serviceName = 'serviceName';

        /** @var iService $mockedService */
        $mockedService = $this->getMockBuilder(iService::class)->getMock();
        $mockedService->expects($this->exactly(2))->method('isSharable')->willReturn(false);
        $mockedService->expects($this->exactly(2))->method('createService')->willReturnCallback(function() {
            return new \stdClass;
        });

        $sm = $this->getServicesContainer();
        $sm->set($serviceName, $mockedService);

        $firstCreatedClass = $sm->get($serviceName);
        $secondCreatedClass = $sm->get($serviceName);

        $this->assertInstanceOf(\stdClass::class, $firstCreatedClass);
        $this->assertInstanceOf(\stdClass::class, $secondCreatedClass);

        $this->assertNotSame($firstCreatedClass, $secondCreatedClass);
    }

    /**
     * @depends testWhenServiceNotRegisteredWillGoThroughRegisteredAggregateServices
     */
    function testServicesCreatedByAggregateServiceWillGetCached()
    {
        $serviceName = 'serviceName';

        $mockedService = $this->getMockBuilder(iFeatureServiceAggregate::class)->getMock();
        // Service will get cloned and name will be changed to the service which is required to be created
        $mockedService->method('isSharable')->willReturn(true);
        $mockedService->expects($this->once())->method('createService')->willReturnCallback(function() {
            return new \stdClass;
        });
        $mockedService->expects($this->once())->method('canCreateService')->with($this->_normalizeName($serviceName))->willReturn(true);
        $mockedService->expects($this->once())->method('withName')->with($this->_normalizeName($serviceName))
            ->willReturn($mockedService);

        $sm = $this->getServicesContainer();
        $sm->set($serviceName, $mockedService);

        $firstCreatedClass = $sm->get($serviceName);
        $secondCreatedClass = $sm->get($serviceName);

        $this->assertInstanceOf(\stdClass::class, $firstCreatedClass);
        $this->assertInstanceOf(\stdClass::class, $secondCreatedClass);

        $this->assertSame($firstCreatedClass, $secondCreatedClass);
    }

    // Implementations

    /**
     * @dataProvider provideValidImplementations
     */
    function testValidImplementationIsGiven($implementation, $storedImplementation)
    {
        $serviceName = 'serviceWouldHaveImplementation';

        $sm = $this->getServicesContainer();
        $sm->setImplementation($serviceName, $implementation);

        $this->assertEquals($storedImplementation, $sm->hasImplementation($serviceName));
    }

    /**
     * @depends testValidImplementationIsGiven
     */
    function testCanChangeImplementationOfServiceWhenNewOneIsExtendingCurrentImplementation()
    {
        $serviceName = 'serviceWouldHaveImplementation';

        $sm = $this->getServicesContainer();
        $sm->setImplementation($serviceName, iService::class);

        // change implementation
        $sm->setImplementation($serviceName, iFeatureServiceAggregate::class);
        $this->assertEquals(iFeatureServiceAggregate::class, $sm->hasImplementation($serviceName));
    }

    /**
     * @dataProvider provideObjectsWithValidImplementations
     */
    function testCreateServicesWithDefinedImplementation($object, $implementation)
    {
        $serviceName = 'ServiceTest';
        $createdService = $object;

        /** @var iService $mockedService */
        $mockedService = $this->getMockBuilder(iService::class)->getMock();
        $mockedService->expects($this->once())->method('createService')->willReturn($createdService);

        $sm = $this->getServicesContainer();
        $sm->setImplementation($serviceName, $implementation);

        $this->assertSame($object, $sm->make($mockedService));
    }

    function testWillThrowExceptionWhenCreatedServiceHasInvalidImplementation()
    {
        $this->expectException(CreateServiceError::class);
        $this->expectExceptionCode(CreateServiceError::ErrorCode_ServiceInvalidImplementation);

        $serviceName = 'ServiceTest';
        $createdService = new \stdClass;

        /** @var iService $mockedService */
        $mockedService = $this->getMockBuilder(iService::class)->getMock();
        $mockedService->expects($this->once())->method('createService')->willReturn($createdService);

        $sm = $this->getServicesContainer();
        $sm->setImplementation($serviceName, \Iterator::class);

        $sm->set($serviceName, $mockedService);
        $sm->fresh($serviceName);
    }

    /**
     * @depends testValidImplementationIsGiven
     */
    function testWillThrowExceptionOnChangeImplementationOfServiceWhenNewOneIsNotExtendingCurrentImplementation()
    {
        $this->expectException(DefineImplementationInvalidArgumentError::class);
        $this->expectExceptionCode(DefineImplementationInvalidArgumentError::ErrorCode_DefinedImplementationCantGetChange);

        $serviceName = 'serviceWouldHaveImplementation';

        $sm = $this->getServicesContainer();
        $sm->setImplementation($serviceName, iService::class);
        // change implementation
        $sm->setImplementation($serviceName, iServicesAware::class);
    }

    /**
     * @depends testServicesCanBeRegisteredSuccessfully
     */
    function testThrowExceptionWhenTryToSetImplementationOfExistingService()
    {
        $this->expectException(DefineImplementationError::class);
        $this->expectExceptionCode(DefineImplementationError::ErrorCode_ServiceAlreadyDefined);

        $serviceName = 'serviceName';

        // Set Service
        /** @var iService $mockedService */
        $mockedService = $this->getMockBuilder(iService::class)->getMock();

        $sm = $this->getServicesContainer();
        $sm->set($serviceName, $mockedService);

        $sm->setImplementation($serviceName, iServicesAware::class);
    }

    /**
     * @dataProvider provideInvalidImplementations
     */
    function testThrowExceptionWhenInvalidImplementationIsGiven($implementation)
    {
        $this->expectException(DefineImplementationInvalidArgumentError::class);
        $this->expectExceptionCode(DefineImplementationError::ErrorCode_InvalidImplementationValue);

        $serviceName = 'serviceWouldHaveImplementation';

        $sm = $this->getServicesContainer();
        $sm->setImplementation($serviceName, $implementation);
    }

    // Aliases

    function testCanGetServiceWithAliasName()
    {
        $serviceName = 'serviceName';
        $createdService = new \stdClass;

        /** @var iService $mockedService */
        $mockedService = $this->getMockBuilder(iService::class)->getMock();
        $mockedService->method('isSharable')->willReturn(true);
        $mockedService->expects($this->once())->method('createService')->willReturnCallback(function() use ($createdService) {
            return $createdService;
        });

        $sm = $this->getServicesContainer();
        $sm->set($serviceName, $mockedService);
        $sm->setAliases($serviceName, \stdClass::class);

        $this->assertSame($createdService, $sm->get(\stdClass::class));
    }

    function testAliasNameCanBeDefinedBeforeActualService()
    {
        $serviceName = 'serviceName';
        $createdService = new \stdClass;

        /** @var iService $mockedService */
        $mockedService = $this->getMockBuilder(iService::class)->getMock();
        $mockedService->method('isSharable')->willReturn(true);
        $mockedService->expects($this->once())->method('createService')->willReturnCallback(function() use ($createdService) {
            return $createdService;
        });

        $sm = $this->getServicesContainer();
        $sm->setAliases($serviceName, \stdClass::class);
        $sm->set($serviceName, $mockedService);

        $this->assertSame($createdService, $sm->get(\stdClass::class));
    }

    function testByAliasingCanReplaceActualServiceWithAnotherService()
    {
        $sm = $this->getServicesContainer();

        /** @var iService $mockedService */
        $mockedService = $this->getMockBuilder(iService::class)->getMock();
        $sm->set('mysql', $mockedService);

        /** @var iService $mockedService */
        $mockedService = $this->getMockBuilder(iService::class)->getMock();
        $mockedService->method('isSharable')->willReturn(true);
        $mockedService->expects($this->once())->method('createService')->willReturn('mongo-gateway');
        $sm->set('mongo', $mockedService);


        $sm->setAliases('mongo', 'mysql');
        $this->assertEquals('mongo-gateway', $sm->get('mysql'));
    }

    function testCanGetServiceWithDefinedImplementationAsAlias()
    {
        $serviceName = 'serviceWouldHaveImplementation';

        /** @var iService $mockedService */
        $mockedService = $this->getMockBuilder(iService::class)->getMock();
        $mockedService->method('isSharable')->willReturn(true);
        $mockedService->expects($this->once())->method('createService')->willReturnCallback(function() {
            return new \stdClass;
        });

        $sm = $this->getServicesContainer();
        $sm->setImplementation($serviceName, \stdClass::class);
        $sm->set($serviceName, $mockedService);

        $createdService = $sm->get($serviceName);
        $aliasService = $sm->get(\stdClass::class);

        $this->assertSame($createdService, $aliasService);
    }

    function testSetServiceWithExistingAliasNameWillReferToOriginService()
    {
        $sm = $this->getServicesContainer();
        $sm->setAliases('originService', 'referToOrigin');

        /** @var iService $mockedOriginService */
        $mockedOriginService = $this->getMockBuilder(iService::class)->getMock();
        $mockedOriginService->method('createService')->willReturn('origin');
        $mockedOriginService->method('isAllowOverride')->willReturn(true);
        $sm->set('originService', $mockedOriginService);

        /** @var iService $mockedAliasService */
        $mockedAliasService = $this->getMockBuilder(iService::class)->getMock();
        $mockedAliasService->method('createService')->willReturn('replaced-origin');
        $sm->set('referToOrigin', $mockedAliasService);

        $this->assertEquals('replaced-origin', $sm->get('originService'));
    }

    function testAliasReferToOriginServiceHasRegistered()
    {
        /** @var iService $mockedOriginService */
        $mockedOriginService = $this->getMockBuilder(iService::class)->getMock();
        $mockedOriginService->method('createService')->willReturn('origin');

        $sm = $this->getServicesContainer();
        $sm->setAliases('originService','referToOrigin');
        $sm->set('originService', $mockedOriginService);

        $this->assertTrue($sm->has('referToOrigin'));
    }

    function testWhenServiceNotRegisteredAggregatorWillReceiveOriginName()
    {
        $serviceName = 'originService';
        /** @var iFeatureServiceAggregate $mockedService */
        $mockedService = $this->getMockBuilder(iFeatureServiceAggregate::class)->getMock();
        $mockedService->method('canCreateService')->with($this->_normalizeName($serviceName))->willReturn(true);

        $sm = $this->getServicesContainer();
        $sm->setAliases('originService', 'referToOrigin');
        $sm->set('AggregateServiceProvider', $mockedService);

        $this->assertTrue($sm->has('referToOrigin'));
    }

    function testCanGetServiceByAliasName()
    {
        /** @var iService $mockedOriginService */
        $mockedOriginService = $this->getMockBuilder(iService::class)->getMock();
        $mockedOriginService->method('createService')->willReturn('origin');

        $sm = $this->getServicesContainer();
        $sm->setAliases('originService', 'referToOrigin');
        $sm->set('originService', $mockedOriginService);

        $this->assertEquals('origin', $sm->get('referToOrigin'));
    }

    function testCanCreateFreshServiceByAliasName()
    {
        /** @var iService $mockedOriginService */
        $mockedOriginService = $this->getMockBuilder(iService::class)->getMock();
        $mockedOriginService->method('createService')->willReturn('origin');

        $sm = $this->getServicesContainer();
        $sm->setAliases('originService','referToOrigin');
        $sm->set('originService', $mockedOriginService);

        $this->assertEquals('origin', $sm->fresh('referToOrigin'));
    }

    function testCanCreateFreshServiceByAliasNameThroughServiceAggregator()
    {
        $serviceName = 'originService';

        /** @var iService $mockedService */
        $mockedService = $this->getMockBuilder(iFeatureServiceAggregate::class)->getMock();
        $mockedService->method('createService')->willReturn('origin');
        $mockedService->method('canCreateService')->with($this->_normalizeName($serviceName))->willReturn(true);
        $mockedService->expects($this->once())->method('withName')->with($this->_normalizeName($serviceName))
            ->willReturn($mockedService);

        $sm = $this->getServicesContainer();
        $sm->setAliases('originService','referToOrigin');
        $sm->set('AggregateServiceProvider', $mockedService);

        $this->assertEquals('origin', $sm->fresh('referToOrigin'));
    }

    function testSetImplementationOfAnAliasWillAffectOriginService()
    {
        $serviceName = 'origin';
        $aliasName = 'referToOrigin';

        $sm = $this->getServicesContainer();
        $sm->setAliases($serviceName, $aliasName);
        $sm->setImplementation($aliasName, \stdClass::class);

        $this->assertEquals(\stdClass::class, $sm->hasImplementation($serviceName));
    }

    function testRecursiveAliases()
    {
        $serviceName = 'serviceName';
        $createdService = new \stdClass;

        /** @var iService $mockedService */
        $mockedService = $this->getMockBuilder(iService::class)->getMock();
        $mockedService->method('isSharable')->willReturn(true);
        $mockedService->expects($this->once())->method('createService')->willReturnCallback(function() use ($createdService) {
            return $createdService;
        });

        $sm = $this->getServicesContainer();
        $sm->set($serviceName, $mockedService);

        $sm->setAliases('serviceName', \stdClass::class);
        $sm->setAliases(\stdClass::class, 'tailAlias');
        $sm->setAliases('tailAlias', 'alias');
        $sm->setAliases('alias', 'headAlias');

        $this->assertSame($createdService, $sm->get('headAlias'));
        $this->assertSame($createdService, $sm->get('alias'));
        $this->assertSame($createdService, $sm->get('tailAlias'));
        $this->assertSame($createdService, $sm->get(\stdClass::class));
        $this->assertSame($createdService, $sm->get('serviceName'));
    }

    function testThrowExceptionWhenThereIsCyclingAliases()
    {
        $this->expectException(CyclingDefinedAliasError::class);
        $this->expectExceptionCode(CyclingDefinedAliasError::ErrorCode_CyclingDefinedAliasesFound);

        /** @var iService $mockedService */
        $mockedService = $this->getMockBuilder(iService::class)->getMock();

        $sm = $this->getServicesContainer();
        $sm->set('serviceName', $mockedService);

        $sm->setAliases('serviceName', \stdClass::class);
        $sm->setAliases(\stdClass::class, 'serviceName');

        $sm->get('serviceName');
    }

    /**
     * @dataProvider provideInvalidAliasNames
     */
    function testThrowExceptionWhenInvalidAliasIsGiven($aliasName, $serviceName)
    {
        $this->expectException(AliasInvalidArgumentError::class);

        $sm = $this->getServicesContainer();
        $sm->setAliases($serviceName, $aliasName);
    }

    // General

    function testInitializerInstance()
    {
        $sm = $this->getServicesContainer();
        $this->assertInstanceOf(Events::class, $sm->events());
    }

    // ..

    function getServicesContainer(): Container
    {
        return new Container;
    }

    function provideDifferentServiceInterfaces()
    {
        return [
            [iService::class],
            [iFeatureServiceAggregate::class],
            [iFeatureServiceEvent::class],
        ];
    }

    function provideObjectsWithValidImplementations()
    {
        return [
            [new \SplQueue, \Iterator::class],
            [new \SplQueue, \Countable::class],
            [new \SplQueue, \Traversable::class],
            [new \SplQueue, \ArrayAccess::class],
            [new \SplQueue, \SplQueue::class],

            [new \Error, \Throwable::class],
            [new \DivisionByZeroError, \Error::class],
            [new \DivisionByZeroError, \ArithmeticError::class],
        ];
    }

    function provideValidImplementations()
    {
        return [
            [new \stdClass, \stdClass::class],
            [\Iterator::class, \Iterator::class],
            [\Traversable::class, \Traversable::class],
            [iServicesAware::class, iServicesAware::class],
        ];
    }

    function provideInvalidImplementations()
    {
        return [
            [\classNotFound::class],
            ['iterable'],
            [12345],
            [true],
            [false],
            [null],
        ];
    }

    function provideInvalidAliasNames()
    {
        return [
            ['similarNames', 'similarNames'],
            ['SimilarNames', 'similarnames'],
            ['similarNames', 'Similarnames'],

            ['similarNames', ''],
            ['', 'Similarnames'],
        ];
    }
}
