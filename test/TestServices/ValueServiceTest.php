<?php
namespace PoirotTest\ServiceManager\TestServices;

use PHPUnit\Framework\TestCase;
use Poirot\ServiceManager\Services\Value;


class ValueServiceTest extends TestCase
{
    /**
     * @dataProvider provideValuableServices
     */
    function testCanCreateServiceSuccessfully($serviceToCreate)
    {
        $value = new Value($serviceToCreate);
        $this->assertEquals($serviceToCreate, $value->createService());
    }

    function testDefaultServiceAttributes()
    {
        $factory = new Value('myService');

        $this->assertEquals(true, $factory->isAllowOverride());
        $this->assertEquals(true, $factory->isSharable());
    }

    // ..

    function provideValuableServices()
    {
        return [
            [new \stdClass],
            [PHP_INT_MAX],
            ['MyStringAsAService'],
        ];
    }
}
