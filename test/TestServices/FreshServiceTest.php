<?php
namespace PoirotTest\ServiceManager\TestServices;

use PHPUnit\Framework\TestCase;
use Poirot\ServiceManager\Container;
use Poirot\ServiceManager\Services\Fresh;


class FreshServiceTest extends TestCase
{
    function testCanCreateServiceSuccessfully()
    {
        $myStdImplementation = new \stdClass;
        $mockedContainer = $this->getMockBuilder(Container::class)->getMock();
        $mockedContainer->expects($this->once())->method('fresh')->with('serviceName')
            ->willReturnCallback(function () use ($myStdImplementation) {
                return $myStdImplementation;
            });

        $fresh = new Fresh('serviceName');
        $fresh->setServicesContainer($mockedContainer);
        $this->assertEquals($myStdImplementation, $fresh->createService());
    }

    function testDefaultServiceAttributes()
    {
        $factory = new Fresh('myService');

        $this->assertEquals(true, $factory->isAllowOverride());
        $this->assertEquals(false, $factory->isSharable());
    }
}
