<?php
namespace PoirotTest\ServiceManager\TestServices;

use PHPUnit\Framework\TestCase;
use Poirot\ServiceManager\Container;
use Poirot\ServiceManager\Interfaces\iServicesContainer;
use Poirot\ServiceManager\Services\Factory;


class FactoryServiceTest extends TestCase
{
    function testCanCreateServiceWithGivenCallable()
    {
        $mockedContainer = $this->getMockBuilder(Container::class)->getMock();

        $serviceToCreate = new \stdClass;
        $factory = new Factory(function (iServicesContainer $container) use ($serviceToCreate, $mockedContainer) {
            $this->assertSame($mockedContainer, $container);
            return $serviceToCreate;
        });

        $factory->setServicesContainer($mockedContainer);

        $this->assertEquals($serviceToCreate, $factory->createService());
    }

    function testDefaultServiceAttributes()
    {
        $factory = new Factory(function () {});

        $this->assertEquals(true, $factory->isAllowOverride());
        $this->assertEquals(true, $factory->isSharable());
    }
}
