<?php
namespace PoirotTest\ServiceManager\TestServices;

use PHPUnit\Framework\TestCase;
use Poirot\ServiceManager\Container;
use Poirot\ServiceManager\Services\Get;


class GetServiceTest extends TestCase
{
    function testCanCreateServiceSuccessfully()
    {
        $myStdImplementation = new \stdClass;
        $mockedContainer = $this->getMockBuilder(Container::class)->getMock();
        $mockedContainer->expects($this->once())->method('get')->with('serviceName')
            ->willReturnCallback(function () use ($myStdImplementation) {
                return $myStdImplementation;
            });

        $get = new Get('serviceName');
        $get->setServicesContainer($mockedContainer);
        $this->assertEquals($myStdImplementation, $get->createService());
    }

    function testDefaultServiceAttributes()
    {
        $factory = new Get('myService');

        $this->assertEquals(true, $factory->isAllowOverride());
        $this->assertEquals(true, $factory->isSharable());
    }
}
