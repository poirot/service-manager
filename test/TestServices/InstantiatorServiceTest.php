<?php
namespace PoirotTest\ServiceManager\TestServices;

use PHPUnit\Framework\TestCase;
use Poirot\ServiceManager\Interfaces\iService;
use Poirot\ServiceManager\Container;
use Poirot\ServiceManager\Services\Instance;
use Poirot\Std\Exceptions\ArgumentResolver\CantResolveParameterDependenciesError;
use PoirotTest\ServiceManager\TestServices\Dummy\ServiceWithTypeHintDependency;
use PoirotTest\ServiceManager\TestServices\Dummy\ServiceWithNotationDependency;


class InstantiatorServiceTest extends TestCase
{
    /**
     * @dataProvider provideTypeHintInstantiableService
     */
    function testCanInjectTypeHintDependencies($instantiable, $instance)
    {
        $mockedContainer = $this->getMockBuilder(Container::class)->getMock();
        $mockedContainer->expects($this->once())
            ->method('has')->with(\stdClass::class)->willReturn(true);
        $mockedContainer->expects($this->once())
            ->method('get')->with(\stdClass::class)->willReturn(new \stdClass);

        $instantiator = new Instance($instantiable);
        $instantiator->setServicesContainer($mockedContainer);

        $this->assertInstanceOf($instance, $instantiator->createService());
    }

    /**
     * @dataProvider provideNotationInstantiableService
     */
    function testCanInjectNotationDependencies($instantiable, $instance)
    {
        $myStdImplementation = new \stdClass;
        $mockedContainer = $this->getMockBuilder(Container::class)->getMock();
        $mockedContainer->expects($this->once())->method('has')->with('myStdImplementation')->willReturn(true);
        $mockedContainer->expects($this->once())->method('get')->with('myStdImplementation')
            ->willReturnCallback(function () use ($myStdImplementation) {
                return $myStdImplementation;
        });

        $instantiator = new Instance($instantiable);
        $instantiator->setServicesContainer($mockedContainer);
        $service = $instantiator->createService();
        $this->assertInstanceOf($instance, $service);
        $this->assertSame($myStdImplementation, $service->resolvedDependency);
    }

    /**
     * @dataProvider provideServiceDependentClasses
     */
    function testGivenOptionsWillBeUsedToInstantiateService($classFqn, $options)
    {
        $mockedContainer = $this->getMockBuilder(Container::class)->getMock();
        $mockedContainer->expects($this->never())->method('has');
        $mockedContainer->expects($this->never())->method('get');

        $instantiator = new Instance($classFqn, $options);
        $instantiator->setServicesContainer($mockedContainer);

        $this->assertInstanceOf($classFqn, $instantiator->createService());
    }

    /**
     * @dataProvider provideServiceDependentClasses
     */
    function testArgumentNameHasHigherPriorityInOptionsWhenResolvingToInstantiable($classFqn, $options)
    {
        $myStdImplementation = new \stdClass;

        $mockedContainer = $this->getMockBuilder(Container::class)->getMock();
        $mockedContainer->expects($this->never())->method('has');
        $mockedContainer->expects($this->never())->method('get');

        $instantiator = new Instance(
            $classFqn,
            $options + [
                'my_std_argument' => $myStdImplementation
            ]
        );
        $instantiator->setServicesContainer($mockedContainer);

        $service = $instantiator->createService();
        $this->assertInstanceOf($classFqn, $service);
        $this->assertSame($myStdImplementation, $service->resolvedDependency);
    }

    /**
     * @dataProvider provideServiceDependentClasses
     */
    function testArgumentNameOptionsCanBeUsedAsAnOnlyAvailableOption($classFqn)
    {
        $myStdImplementation = new \stdClass;

        $mockedContainer = $this->getMockBuilder(Container::class)->getMock();
        $mockedContainer->method('has')->willReturn(false);
        $mockedContainer->expects($this->never())->method('get');

        $instantiator = new Instance(
            $classFqn,
            [
                'my_std_argument' => $myStdImplementation
            ]
        );
        $instantiator->setServicesContainer($mockedContainer);

        $service = $instantiator->createService();
        $this->assertInstanceOf($classFqn, $service);
        $this->assertSame($myStdImplementation, $service->resolvedDependency);
    }

    function testCanUseServiceAsAnInstanceParamsOption()
    {
        /** @var iService $mockedService */
        $mockedService = $this->getMockBuilder(iService::class)->getMock();
        $mockedService->expects($this->once())->method('createService')->willReturn(new \stdClass);

        $mockedContainer = $this->getMockBuilder(Container::class)->getMock();
        $mockedContainer->expects($this->once())->method('make')->with($mockedService)
            ->willReturnCallback(function ($mockedService) {
                return $mockedService->createService();
        });


        $instantiator = new Instance(ServiceWithTypeHintDependency::class, [
            \stdClass::class => $mockedService
        ]);
        $instantiator->setServicesContainer($mockedContainer);

        $this->assertInstanceOf(ServiceWithTypeHintDependency::class, $instantiator->createService());
    }

    /**
     * @dataProvider provideServiceDependentClasses
     */
    function testThrowExceptionIfCantResolveRequiredArguments($classFqn)
    {
        $this->expectException(CantResolveParameterDependenciesError::class);
        $mockedContainer = $this->getMockBuilder(Container::class)->getMock();
        $mockedContainer->method('has')->willReturn(false);

        $instantiator = new Instance($classFqn, []);
        $instantiator->setServicesContainer($mockedContainer);

        $instantiator->createService();
    }

    function testDefaultServiceAttributes()
    {
        $factory = new Instance(\stdClass::class);

        $this->assertEquals(true, $factory->isAllowOverride());
        $this->assertEquals(true, $factory->isSharable());
    }

    // ..

    function provideServiceDependentClasses()
    {
        return [
            [ServiceWithNotationDependency::class, ['myStdImplementation' => new \stdClass]],
            [ServiceWithTypeHintDependency::class, [\stdClass::class => new \stdClass]],
        ];
    }

    function provideTypeHintInstantiableService()
    {
        return [
            [ServiceWithTypeHintDependency::class, ServiceWithTypeHintDependency::class],
            [[ServiceWithTypeHintDependency::class, 'create'], ServiceWithTypeHintDependency::class],
        ];
    }

    function provideNotationInstantiableService()
    {
        return [
            [ServiceWithNotationDependency::class, ServiceWithNotationDependency::class],
            [[ServiceWithNotationDependency::class, 'create'], ServiceWithNotationDependency::class],
        ];
    }
}
