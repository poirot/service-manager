<?php
namespace PoirotTest\ServiceManager\TestServices\Dummy;


class ServiceWithTypeHintDependency
{
    public $resolvedDependency;

    static function create(\stdClass $myStdService)
    {
        return new self($myStdService);
    }

    function __construct(\stdClass $myStdArgument)
    {
        // \stdClass as dependency should be resolved to this class
        $this->resolvedDependency = $myStdArgument;
    }
}
