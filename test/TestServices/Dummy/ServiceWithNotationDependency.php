<?php
namespace PoirotTest\ServiceManager\TestServices\Dummy;


class ServiceWithNotationDependency
{
    public $resolvedDependency;

    /**
     * Factory
     *
     * @param \stdClass $myStdService @Service myStdImplementation
     * @return ServiceWithNotationDependency
     */
    static function create(\stdClass $myStdService)
    {
        return new self($myStdService);
    }

    /**
     * Constructor
     *
     * @param \stdClass $myStdArgument @Service myStdImplementation
     */
    function __construct(\stdClass $myStdArgument)
    {
        // \stdClass as dependency should be resolved to this class
        $this->resolvedDependency = $myStdArgument;
    }
}
